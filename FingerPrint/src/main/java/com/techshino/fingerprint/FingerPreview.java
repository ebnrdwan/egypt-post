package com.techshino.fingerprint;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;

public class FingerPreview {

	public FingerJNI fingerJNI;
	protected Context mContext;
	protected Handler mhandler;
	public boolean isworking = false;
	public boolean isregisted = false;
	public float score;
	private int current_PID = 0x64ab;

	private int ignorelatestimage = 0;// 视频流会有延迟

	public static byte[] grey_bmp_buffer = new byte[256 * 360 + 1024 + 54];
	public static byte[] feature = new byte[513];
	public static byte[] feature0 = new byte[513];
	public static byte[] feature1 = new byte[513];
	public static byte[] feature2 = new byte[513];
	public static byte[] feature_templet = new byte[513];
	public static byte[] grey_bmpdata_buffer = new byte[256 * 360 * 4 / 3];
	public int outoftime_count = 0;
	
	public static int nfiqValue = 0;
	

	public FingerPreview(Context context, Handler mhandler) {
		fingerJNI = new FingerJNI();
		this.mContext = context;
		this.mhandler = mhandler;
		outoftime_count = 0;
		if (checkusbdevice()) {
			mhandler.sendEmptyMessage(3);
		} else {
			mhandler.sendEmptyMessage(-3);
		}

	}

	private boolean checkusbdevice() {
		Log.e("checkusbdevice", "checkusbdevice  in ");
		UsbManager usbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
		HashMap<String, UsbDevice> devlist = usbManager.getDeviceList();
		Iterator<UsbDevice> devs = devlist.values().iterator();
		while (devs.hasNext()) {
			UsbDevice d = devs.next();
			Log.e("checkusbdevice", "d.getProductId() = " + String.format("%04X", d.getProductId()));
			Log.e("checkusbdevice", "d.getVendorId() = " + String.format("%04X", d.getVendorId()));
			if ((d.getProductId() == current_PID) || (0x735f == d.getVendorId())) {
				Log.e("checkusbdevice", "found TCO310 device");
				return true;
			}
		}
		return false;
	}

	public int Init() {
		int ret = fingerJNI.TCFP_Init(1, 1, "/storage/emulated/legacy");
		if (ret == -100) {
			mhandler.sendEmptyMessage(-100);
		}
		if (ret >= 0) {
			mhandler.sendEmptyMessage(3);
		} else {
			mhandler.sendEmptyMessage(-3);
		}
		return ret;

	}

	//提取图像和特征值
	public int StartExtractFeatureAndImageThread() {
		isworking = true;
		ignorelatestimage = 0;
		new Thread(new Runnable() {

			@Override
			public void run() {

				while (isworking) {
					fingerJNI.TCFP_ImageExtract(grey_bmp_buffer, grey_bmpdata_buffer);
					int[]imageAttr = new int[]{256, 360, 500};
					int QScore = fingerJNI.TCFP_GetQualityScore(grey_bmpdata_buffer, grey_bmpdata_buffer.length, imageAttr);
					Log.e("TC", "StartExtractFeatureAndImageThread score = "+String.valueOf(score));
					nfiqValue = QScore;
					if(QScore <= FPConfig.getQualitythreshold()){
						int result = fingerJNI.TCFP_FeatureExtract(feature, grey_bmpdata_buffer, grey_bmpdata_buffer.length, imageAttr);
						Log.e("TC", "StartExtractFeatureAndImageThread TCFP_FeatureExtract = "+String.valueOf(result));
					}
					if (ignorelatestimage < 1) {
						ignorelatestimage++;
						continue;
					} else {
						mhandler.sendEmptyMessage(0);
					}
				}
			}
		}).start();
		return 0;
	}

	static int regTimes = 0;
	static boolean isPressing = false;
	protected void savefeature(byte[] feature, int len) {
		switch (regTimes) {
		case 0:
			mhandler.sendEmptyMessage(101);
			System.arraycopy(feature, 0, feature0, 0, len);
			break;
		case 1:
			mhandler.sendEmptyMessage(102);
			System.arraycopy(feature, 0, feature1, 0, len);
			break;
		case 2:
			System.arraycopy(feature, 0, feature2, 0, len);
			break;
		default:
		}
		regTimes++;
	}
	//注册的线程示例 
	public int StartRegistTempletThread() {
		isworking = true;
		ignorelatestimage = 0;
		outoftime_count = 0;
		regTimes = 0; //记录注册第n次 
		isPressing = false; //默认抬手
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isworking) {
					fingerJNI.TCFP_ImageExtract(grey_bmp_buffer, grey_bmpdata_buffer);
					int[]imageAttr = new int[]{256, 360, 500};
					int QScore = fingerJNI.TCFP_GetQualityScore(grey_bmpdata_buffer, grey_bmpdata_buffer.length, imageAttr);
					Log.e("TC", "StartRegistTempletThread QScore = "+String.valueOf(score));
					nfiqValue = QScore;
					
					
					if(QScore <= FPConfig.getQualitythreshold()){ //按下
						if (ignorelatestimage <= 0) { //首先清除帧缓冲buffer
							ignorelatestimage++;
							continue;
						}
						if(isPressing) {
							mhandler.sendEmptyMessage(-10);
							continue; //其次，如果已经是按下状态，需要通过一次抬手
						}
						isPressing = true;
						int result = fingerJNI.TCFP_FeatureExtract(feature, grey_bmpdata_buffer, grey_bmpdata_buffer.length, imageAttr);
						savefeature(feature, result); //save feature
						if(regTimes == 3){
							mhandler.sendEmptyMessage(0);
							mhandler.sendEmptyMessage(1);
							int len = fingerJNI.TCFP_FeatureTempletExtract(feature0, feature1, feature2, feature);
							if(len > 0)
								System.arraycopy(feature, 0, feature_templet, 0, len); //save feature
							else
								mhandler.sendEmptyMessage(-1);
							isworking = false;
						}else{
							mhandler.sendEmptyMessage(0);
							mhandler.sendEmptyMessage(-10);
						}
					}else{//抬手
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						isPressing = false; //抬手
						mhandler.sendEmptyMessage(10);
						if ((outoftime_count++) >= 60) {
							isworking = false;
							mhandler.sendEmptyMessage(-13);
							break;
						}
					}
				}
			}
		}).start();
		return 0;

	}

	//比对的线程示例
	public int StartCompareFeatureThread() {
		isworking = true;
		ignorelatestimage = 0;
		outoftime_count = 0;

		new Thread(new Runnable() {

			@Override
			public void run() {

				while (isworking) {
					fingerJNI.TCFP_ImageExtract(grey_bmp_buffer, grey_bmpdata_buffer);
					int[]imageAttr = new int[]{256, 360, 500};
					int QScore = fingerJNI.TCFP_GetQualityScore(grey_bmpdata_buffer, grey_bmpdata_buffer.length, imageAttr);
					Log.e("TC", "StartCompareFeatureThread QScore = "+String.valueOf(QScore));
					nfiqValue = QScore;
					if(QScore <= FPConfig.getQualitythreshold()){
						int res = fingerJNI.TCFP_FeatureExtract(feature, grey_bmpdata_buffer, grey_bmpdata_buffer.length, imageAttr);
						if (ignorelatestimage <= 0) {
							ignorelatestimage++;
							continue;
						}
						score = fingerJNI.TCFP_FeatureMatch(feature, feature.length, feature_templet, feature_templet.length);
						if (score > FPConfig.getCompareScorethreshold()) {
							mhandler.sendEmptyMessage(0);
							mhandler.sendEmptyMessage(2);
						} else {
							mhandler.sendEmptyMessage(-2);
						}
						isworking = false;
						break;
					} else {
						if ((outoftime_count++) >= 10) {
							isworking = false;
							mhandler.sendEmptyMessage(-13);
							break;
						}
					}

				}

			}
		}).start();

		return 0;

	}

	private void savebitmap(byte[] bitmap) {

		try {
			// 存储文件名
			int i = 0;
			String filename;
			File file;
			while (true) {
				filename = "/sdcard/teso/";
				file = new File(filename);
				if (!file.exists())
					file.mkdir();
				filename = "/sdcard/teso/teso" + i + ".bmp";
				file = new File(filename);

				if (file.exists()) {
					i++;
					continue;

				}

				else {

					break;
				}

			}
			FileOutputStream fileos = new FileOutputStream(filename);

			fileos.write(bitmap);
			fileos.flush();
			fileos.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

 

}

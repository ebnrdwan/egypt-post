package com.techshino.fingerprint;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class FingerprintActivity extends Activity {
	FingerPreview fingercamera;

	private Button btnExtractFeatureAndImage, btnRegisterTemplet,
			btnCompareFeature;
	private TextView tvInfo, tvInfo1, tvNFIQ;
	private ImageView imageViewFingerPrint;
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.finger);
		imageViewFingerPrint = (ImageView) findViewById(R.id.imageView_FingerPrint);
		btnExtractFeatureAndImage = (Button) findViewById(R.id.btn_ExtractFeatureAndImage);
		btnRegisterTemplet = (Button) findViewById(R.id.btn_RegisterTemplet);
		btnCompareFeature = (Button) findViewById(R.id.btn_CompareFeature);
		tvInfo = (TextView) findViewById(R.id.tV_info);
		tvInfo1 = (TextView) findViewById(R.id.tV_info1);
		tvNFIQ = (TextView) findViewById(R.id.tV_info2);
		
		setTitle("TCO310 TcFpMinex");
		//Algorithm Config for STQC
		FPConfig.setFeatureType(FPConfig.FEATURE_INTERNATIONAL_ISO_19794_2_2011);
		FPConfig.setFingerCode(FPConfig.Unknown);
		FPConfig.setQualitythreshold(FPConfig.NFIQ_GOOD);
		FPConfig.setCompareScorethreshold(1200);
		//root for chmod video 0~15
	    new Thread(new Runnable() {
			
			@Override
			public void run() {
				String path = "/dev/video";
				for (int i = 0; i < 15; i++) {
					Utils.upgradeRootPermission(path + i);
				}
			}
		});

		btnExtractFeatureAndImage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (fingercamera.isworking) {
						tvInfo.setText(R.string.endCapturingimage);
						setallbuttontrue();
						fingercamera.isworking = false;
						btnExtractFeatureAndImage.setText("Monitor Mode");
					} else {
						setallbuttonfalse();
						btnExtractFeatureAndImage.setEnabled(true);
						tvInfo.setText(R.string.Capturingimage);
						btnExtractFeatureAndImage.setText("stop Monitor");
						fingercamera.StartExtractFeatureAndImageThread();
					}

				}
		});
		btnRegisterTemplet.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				fingercamera.StartRegistTempletThread();
				setallbuttonfalse();

			}
		});
		btnCompareFeature.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tvInfo.setText(R.string.Matching);
				fingercamera.StartCompareFeatureThread();
				setallbuttonfalse();

			}
		});

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		btnExtractFeatureAndImage.setText("Monitor Mode");
		fingercamera = new FingerPreview(this, myHanlder);
		new Thread(new Runnable() {

			@Override
			public void run() {
				fingercamera.Init();
				fingercamera.Init();

			}
		}).start();
		detectUsbDeviceAttach();
		Log.e("app", "onResume");
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.e("app", "onPause");
		fingercamera.isworking = false;

		new Thread(new Runnable() {

			@Override
			public void run() {
				fingercamera.fingerJNI.TCFP_UnInit();
			}
		}).start();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mUsbReceiver);
	}

	private void setallbuttontrue() {
		btnExtractFeatureAndImage.setEnabled(true);
		btnRegisterTemplet.setEnabled(true);
		if (fingercamera.isregisted) {
			btnCompareFeature.setEnabled(true);
		} else {
			btnCompareFeature.setEnabled(false);
		}
	}

	private void setallbuttonfalse() {
		btnExtractFeatureAndImage.setEnabled(false);
		btnRegisterTemplet.setEnabled(false);
		btnCompareFeature.setEnabled(false);
	}
 
	private void detectUsbDeviceAttach() {
		// listen usb device attach
		IntentFilter usbDeviceStateFilter = new IntentFilter();

		usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
		usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
		usbDeviceStateFilter
				.addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED);
		usbDeviceStateFilter
				.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);

		registerReceiver(mUsbReceiver, usbDeviceStateFilter);

	}

	BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			String curItentActionName = intent.getAction();

			if (UsbManager.ACTION_USB_DEVICE_ATTACHED
					.equals(curItentActionName)) {
				String path = "/dev/video";
				for (int i = 0; i < 15; i++) {
					Utils.upgradeRootPermission(path + i);
				}
				new Thread(new Runnable() {
					@Override
					public void run() {
						fingercamera.Init();
					}
				}).start();
				btnExtractFeatureAndImage.setText("Monitor Mode");
				tvInfo.setText(R.string.connectsuccess);
				tvInfo1.setText("256*360");
				fingercamera.isworking = false;

			} else if (UsbManager.ACTION_USB_DEVICE_DETACHED
					.equals(curItentActionName)) {
				setallbuttonfalse();
				fingercamera.fingerJNI.TCFP_UnInit();
				fingercamera.isworking = false;
				tvInfo.setText(R.string.Devicedisconnected);
				// tvInfo1.setText("width*height");
				fingercamera.isworking = false;
			}
		}

	};


	// TODO: 11/8/18 where images is drawing
	public Handler myHanlder = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				Bitmap bmFinger;
				bmFinger = BitmapFactory.decodeByteArray(
						fingercamera.grey_bmp_buffer, 0, 256 * 360 + 1024 + 54);
				imageViewFingerPrint.setImageBitmap(bmFinger);
				tvNFIQ.setText("NFIQ: "+ String.valueOf(fingercamera.nfiqValue));
				break;
			case 1:
				tvInfo.setText(R.string.Enrollmentsucceeded);
				fingercamera.isregisted = true;
				setallbuttontrue();
				break;

			case -1:
				tvInfo.setText(R.string.regfail);
				fingercamera.isregisted = false;
				setallbuttontrue();
				break;
			case 2:
				tvInfo.setText(getString(R.string.Matchingsucceeded)
						+ fingercamera.score);
				setallbuttontrue();
				break;
			case -2:
				tvInfo.setText(getString(R.string.Matchingfailed)
						+ " socre : " + fingercamera.score);
				imageViewFingerPrint.setImageBitmap(BitmapFactory.decodeByteArray(
						fingercamera.grey_bmp_buffer, 0, 256 * 360 + 1024 + 54));
				tvNFIQ.setText("NFIQ: "+ String.valueOf(fingercamera.nfiqValue));
				setallbuttontrue();
				break;
			case 3:
				tvInfo.setText(R.string.founduvcdevice);

				setallbuttontrue();

				break;
			case -3:
				tvInfo.setText(R.string.notfounduvcdevice);

				setallbuttonfalse();
				break;
			case 10:
				tvInfo.setText(R.string.readytoreadagain);
				break;
			case -10:
				tvInfo.setText(R.string.movefingeraway);
				break;
			case -13:
				tvInfo.setText(R.string.outoftime);
				setallbuttontrue();
				break;
			case 101:
				tvInfo.setText(R.string.twolefttoreg);
				break;
			case 102:
				tvInfo.setText(R.string.onelefttoreg);
				break;

			}

		}
	};

}

package egyptpost.app.paysky.egyptpost.view.activity

import android.os.Bundle
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity
import egyptpost.app.paysky.egyptpost.base.extentions.scanIdSetup
import egyptpost.app.paysky.egyptpost.base.extentions.setMyTitle
import egyptpost.app.paysky.egyptpost.base.extentions.setTransBg
import egyptpost.app.paysky.egyptpost.view.fragment.CashWithdrowFragment

class WithdrawActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_withdraw
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        toolbar?.setTransBg(this)
        toolbar?.scanIdSetup()
        toolbar?.setMyTitle(R.string.cash_with)

        val withdrawFragment = CashWithdrowFragment()
        replaceFragment(withdrawFragment, true)
    }
}

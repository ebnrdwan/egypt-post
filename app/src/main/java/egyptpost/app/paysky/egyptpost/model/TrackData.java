package egyptpost.app.paysky.egyptpost.model;

/**
 * Created by PaySky-3 on 8/13/2017.
 */

public class TrackData {
    String data;
    String PAN;
    String track2;
    String[] tracks;
    String CardNumber;
    String ExpiryData;


    public TrackData(String data) {
        if ((data.charAt(data.length() - 1) == 'f') || (data.charAt(data.length() - 1) == 'F'))
            data = data.substring(0, data.length() - 1);

        /*track2equivalan */
        if (data.contains("d")) {
            tracks = data.split("d");
            CardNumber = tracks[0];
            ExpiryData = tracks[1].substring(0, 4);
        } else {
            /*track2 magnitic*/
//            tracks = data.split("\n");
//            String[] track2Spilt = track2.split(":");
            tracks = data.split("=");
            CardNumber = tracks[0];
            ExpiryData = tracks[1].substring(0, 4);
        }


        if (tracks.length == 0) {
            return;
        }
        PAN = tracks[0];
        if (tracks.length == 1) {
            return;
        }
//        track2 = tracks[1];
//        if (track2.contains(":")) {
//            String[] track2Spilt = track2.split(":");
//            CardNumber = track2Spilt[1].split("=")[0];
//            ExpiryData = track2Spilt[1].split("=")[1].substring(0, 4);
//        } else {
//            String[] track2Spilt = track2.split(":");
//            CardNumber = track2Spilt[1].split("=")[0];
//            ExpiryData = track2Spilt[1].split("=")[1].substring(0, 4);
//        }
    }

    public TrackData(String data, boolean x) {
        if (!data.contains("d")) return;
        tracks = data.split("d");
        if (tracks.length == 0) {
            return;
        }
        PAN = tracks[0];
        if (tracks.length == 1) {
            return;
        }
        CardNumber = tracks[0];
        ;
        ExpiryData = tracks[1].substring(0, 4);
    }


    public String getCardNumber() {
        return CardNumber;
    }

    public String getExpiryData() {
        return ExpiryData;
    }

    public int getTrackLenght() {
        return tracks.length;
    }


}

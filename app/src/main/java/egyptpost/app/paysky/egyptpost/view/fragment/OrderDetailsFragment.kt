package egyptpost.app.paysky.egyptpost.view.fragment


import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.device.ScanManager
import android.media.AudioManager
import android.media.SoundPool
import android.os.Bundle
import android.os.Vibrator
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.view.activity.PickOrderActivity
import kotlinx.android.synthetic.main.fragment_order_details.*


class OrderDetailsFragment : Fragment() {


    private var pick: PickOrderActivity? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pick = activity as PickOrderActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order_details, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pick?.onCountChanged(2)

        shipping_details_view.setOnClickListener {
            val shippingDetailsFragment = ShippingDetailsFragment();
            pick?.replaceFragment(shippingDetailsFragment, true)


        }

        scan_barcode_ll.setOnClickListener {
            mScanManager?.stopDecode()
            isScaning = true
            try {
                Thread.sleep(100)
            } catch (e: InterruptedException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

            mScanManager?.startDecode()
        }
    }

    private val SCAN_ACTION = "urovo.rcv.message"//扫描结束action

    private var mVibrator: Vibrator? = null
    private var mScanManager: ScanManager? = null
    private var soundpool: SoundPool? = null
    private var soundid: Int = 0
    private var barcodeStr: String? = null
    private var isScaning = false
    private val mScanReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            // TODO Auto-generated method stub
            isScaning = false
            soundpool!!.play(soundid, 1f, 1f, 0, 0, 1f)
            val barcode = intent.getByteArrayExtra("barocode")
            //byte[] barcode = intent.getByteArrayExtra("barcode");
            val barocodelen = intent.getIntExtra("length", 0)
            val temp = intent.getByteExtra("barcodeType", 0.toByte())
            android.util.Log.i("debug", "----codetype--$temp")
            barcodeStr = String(barcode, 0, barocodelen)

            showScanResult.setText(barcodeStr)

        }

    }


    private fun initScan() {
        // TODO Auto-generated method stub
        mScanManager = ScanManager()
        mScanManager!!.openScanner()

        mScanManager!!.switchOutputMode(0)
        soundpool = SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100) // MODE_RINGTONE
        soundid = soundpool!!.load("/etc/Scan_new.ogg", 1)
    }


    override fun onPause() {
        // TODO Auto-generated method stub
        super.onPause()
        if (mScanManager != null) {
            mScanManager!!.stopDecode()
            isScaning = false
        }
        activity?.unregisterReceiver(mScanReceiver)
    }


    override fun onResume() {
        // TODO Auto-generated method stub
        super.onResume()
        initScan()
        showScanResult.setText("")
        val filter = IntentFilter()
        filter.addAction(SCAN_ACTION)
        activity?.registerReceiver(mScanReceiver, filter)
    }


}

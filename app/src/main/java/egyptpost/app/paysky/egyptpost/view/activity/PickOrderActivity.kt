package egyptpost.app.paysky.egyptpost.view.activity

import android.os.Bundle
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity
import egyptpost.app.paysky.egyptpost.base.extentions.setNavNonBackMode
import egyptpost.app.paysky.egyptpost.base.extentions.setTransBg
import egyptpost.app.paysky.egyptpost.view.fragment.AddOrderFragment
import kotlinx.android.synthetic.main.activity_add_order.*

class PickOrderActivity : BaseActivity(), BaseActivity.onFragmentCountsChanged {
    override fun onCountChanged(count: Int) {
        seekBar.setStep(count)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_add_order;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        val addOrderFragment = AddOrderFragment();
        setFragmentCountListener(this)
        replaceFragment(addOrderFragment, false);
        setupToolbar()
        toolbar?.setTransBg(this)
        toolbar?.setNavNonBackMode(false)


    }


}

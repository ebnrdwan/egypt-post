package egyptpost.app.paysky.egyptpost.model

data class ServiceModel(val type: Number, var name: String, var icon: Int, var color: Int) {
}
package egyptpost.app.paysky.egyptpost.base.extentions

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.utils.Constants


fun String.colorText(color: String): String {


    var input = "<font color=" + color + ">" + this + "</font>";
    return input;
}


fun String.getColorHexa(context: Context?): String {
    var hexa = when (this) {
        Constants.primary -> return context?.resources?.getString(R.color.colorPrimary) ?: ""
        Constants.primaryDark -> return context?.resources?.getString(R.color.colorPrimaryDark) ?: ""
        Constants.accent -> return context?.resources?.getString(R.color.colorAccent) ?: ""
        else -> return "#000"
    }
}

inline fun FragmentActivity.replaceFragment(containerViewId: Int, f: () -> Fragment): Fragment? {
    return f().apply { supportFragmentManager?.beginTransaction()?.replace(containerViewId, this)?.commit() }
}
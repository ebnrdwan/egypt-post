package egyptpost.app.paysky.egyptpost.view.dialog;

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.view.dialog.listener.OnCardReadListener
import kotlinx.android.synthetic.main.dialog_scan_card.*

class CardScanDialog(context: Context, var cardReadListener: OnCardReadListener) : Dialog(context), Runnable {
    lateinit var handle: Handler
    var index = 1
    override fun run() {

        if (index == 2) dismiss();

        if (index == 1) {
            card_imageView.setImageResource(R.drawable.ic_read_card_blue);
            card_textView.setText(R.string.reading_card);
            handle.postDelayed(this, 1000)
            index++;
        }
    }


    init {
        initView();
    }


    private fun initView() {

        val lp = WindowManager.LayoutParams()
        window!!.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        lp.copyFrom(window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        lp.gravity = Gravity.CENTER
        window!!.attributes = lp
        val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_scan_card, null);
        setContentView(dialogView);
        handle = Handler();
    }


    override fun show() {
        super.show();
        handle.postDelayed(this, 2000);
    }
    override fun dismiss() {
        super.dismiss();
        cardReadListener.onReadCardSuccess();
    }
}
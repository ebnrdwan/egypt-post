package egyptpost.app.paysky.egyptpost.view.activity

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity
import egyptpost.app.paysky.egyptpost.base.extentions.scanIdSetup
import kotlinx.android.synthetic.main.scan_id_activity.*

class ScanIdActivity : BaseActivity(), View.OnClickListener {

    val REQUEST_IMAGE_CAPTURE = 1

    override fun getLayoutId(): Int {
        return R.layout.scan_id_activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        toolbar?.scanIdSetup()

    }


    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }



    override fun onClick(view: View?) {

    }

}

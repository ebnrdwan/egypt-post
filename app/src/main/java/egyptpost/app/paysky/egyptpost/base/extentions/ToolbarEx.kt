package egyptpost.app.paysky.egyptpost.base.extentions

import android.content.Context
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import egyptpost.app.paysky.egyptpost.R

fun Toolbar.setMyTitle(titleString: String): Toolbar {
    var title = this.findViewById<TextView>(R.id.tb_title);
    title?.setText(titleString)
    return this
}

fun Toolbar.setMyTitle(titleInt: Int): Toolbar {
    val titlestring = resources.getString(titleInt)
    val title = this.findViewById<TextView>(R.id.tb_title);
    title?.setText(titlestring)
    return this
}


fun Toolbar.setSecondTitle(titleString: String): Toolbar {
    var title = this.findViewById<TextView>(R.id.tb_second_title);
    title?.text = titleString
    return this
}

fun Toolbar.setSecondTitle(titleInt: Int): Toolbar {
    val titlestring = resources.getString(titleInt)
    val title = this.findViewById<TextView>(R.id.tb_second_title);
    title?.text = titlestring
    return this
}


fun Toolbar.setSecondVisibility(visibile: Boolean): Toolbar {
    var title = this.findViewById<View>(R.id.tb_second_title);
    if (visibile) title?.visibility = View.VISIBLE;
    else title?.visibility = View.GONE;
    return this
}


fun Toolbar.setMiddleIconVisibility(visibile: Boolean): Toolbar {
    var title = this.findViewById<View>(R.id.tb_rout_icon);
    if (visibile) title?.visibility = View.VISIBLE;
    else title?.visibility = View.GONE;
    return this
}


fun Toolbar.setTitleVisiblity(visibile: Boolean): Toolbar {
    var title = this.findViewById<View>(R.id.tb_title);
    if (visibile) title?.visibility = View.VISIBLE;
    else title?.visibility = View.GONE;
    return this
}

fun Toolbar.setNavNonBackMode(mNav: Boolean): Toolbar {
    val nav = this.findViewById<View>(R.id.tb_nav);
    val back = this.findViewById<View>(R.id.tb_back);
    if (mNav) {
        nav.visibility = View.VISIBLE;
        back.visibility = View.GONE;
    } else {
        nav.visibility = View.GONE;
        back.visibility = View.VISIBLE; }
    return this
}

fun Toolbar.setTransBg(context: Context) {
    this.background = resources?.getDrawable(R.drawable.bg_trans)
}

fun Toolbar.setToolbarBg(context: Context) {
    this.background = resources?.getDrawable(R.drawable.bg_toolbar)
}

fun Toolbar.setNotificationVisiblitiy(visibile: Boolean): Toolbar {
    var nav = this.findViewById<RelativeLayout>(R.id.tb_notification);

    if (visibile) {
        nav?.visibility = View.VISIBLE;

    } else {
        nav?.visibility = View.GONE;
    }
    return this
}

fun Toolbar.fullSetub() {
    setTitleVisiblity(true)
    setSecondVisibility(true)
    setNavNonBackMode(true)
    setNotificationVisiblitiy(true)
    setMiddleIconVisibility(true)
}

fun Toolbar.loginSetup() {
    setTitleVisiblity(false)
    setSecondVisibility(false)
    setNavNonBackMode(true)
    setNotificationVisiblitiy(false)
    setMiddleIconVisibility(false)
}

fun Toolbar.scanIdSetup() {
    setMyTitle(R.string.cash_deposite_title)
    setSecondTitle(R.string.cancel)
    setMiddleIconVisibility(false)
    setNavNonBackMode(false)
}

//  todo build builder for building and customizing toolbar
//  todo handle generics
// todo handle set icons and position












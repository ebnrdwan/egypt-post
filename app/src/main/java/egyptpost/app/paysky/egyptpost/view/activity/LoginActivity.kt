package egyptpost.app.paysky.egyptpost.view.activity

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.hardware.usb.UsbManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import com.techshino.fingerprint.FPConfig
import com.techshino.fingerprint.FingerPreview
import com.techshino.fingerprint.Utils
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity
import egyptpost.app.paysky.egyptpost.base.extentions.loginSetup
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.head_view.*

class LoginActivity : BaseActivity() {

    lateinit var fingercamera: FingerPreview

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        theme = R.style.SplashThem
        super.onCreate(savedInstanceState)
        setupToolbar()
        toolbar?.loginSetup()
        headView_logo.visibility = View.VISIBLE

        initiFunctions()
        iv_finger.setOnClickListener {

        }
        tv_messag.setOnClickListener {
//            fingercamera.StartRegistTempletThread()
            launchScreen(DashboardActivity().javaClass, false);
        }

        //Algorithm Config for STQC
        FPConfig.setFeatureType(FPConfig.FEATURE_INTERNATIONAL_ISO_19794_2_2011)
        FPConfig.setFingerCode(FPConfig.Unknown)
        FPConfig.setQualitythreshold(FPConfig.NFIQ_GOOD)
        FPConfig.setCompareScorethreshold(1200)
        //root for chmod video 0~15
        Thread(Runnable {
            val path = "/dev/video"
            for (i in 0..14) {
                Utils.upgradeRootPermission(path + i)
            }
        })


    }


    fun initiFunctions() {
        FPConfig.setFeatureType(FPConfig.FEATURE_INTERNATIONAL_ISO_19794_2_2011)
        FPConfig.setFingerCode(FPConfig.Unknown)
        FPConfig.setQualitythreshold(FPConfig.NFIQ_GOOD)
        FPConfig.setCompareScorethreshold(1200)
        //root for chmod video 0~15
        Thread(Runnable {
            val path = "/dev/video"
            for (i in 0..14) {
                Utils.upgradeRootPermission(path + i)
            }
        })


    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        resources.updateConfiguration(newConfig, resources.displayMetrics)
    }


    override fun onResume() {
        // TODO Auto-generated method stub
        super.onResume()
        fingercamera = FingerPreview(this, myHanlder)
        Thread(Runnable {
            fingercamera.Init()
            fingercamera.Init()
        }).start()
        detectUsbDeviceAttach()
        Log.e("app", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("app", "onPause")
        fingercamera.isworking = false

        Thread(Runnable { fingercamera.fingerJNI.TCFP_UnInit() }).start()

    }


    internal var mUsbReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            val curItentActionName = intent.action

            if (UsbManager.ACTION_USB_DEVICE_ATTACHED == curItentActionName) {
                val path = "/dev/video"
                for (i in 0..14) {
                    Utils.upgradeRootPermission(path + i)

                }
                Thread(Runnable { fingercamera.Init() }).start()
                tv_messag.setText(com.techshino.fingerprint.R.string.connectsuccess)
                tv_messag.setTextColor(resources.getColor(R.color.green))
                fingercamera.isworking = false

            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED == curItentActionName) {
                setallbuttonfalse()
                fingercamera.fingerJNI.TCFP_UnInit()
                fingercamera.isworking = false
                tv_messag.setText(com.techshino.fingerprint.R.string.Devicedisconnected)
                // tv_messag.setText("width*height");
                tv_messag.setTextColor(resources.getColor(R.color.red))
                fingercamera.isworking = false
            }
        }

    }

    var myHanlder: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                0 -> {
                    val bmFinger: Bitmap
                    bmFinger = BitmapFactory.decodeByteArray(
                            com.techshino.fingerprint.FingerPreview.grey_bmp_buffer, 0, 256 * 360 + 1024 + 54)
                    if (bmFinger != null)
                        iv_finger.setImageBitmap(bmFinger)
                    tv_messag.setText("NFIQ: " + com.techshino.fingerprint.FingerPreview.nfiqValue.toString())
                    tv_messag.setTextColor(resources.getColor(R.color.font_dark))
                }
                1 -> {
                    tv_messag.setText(com.techshino.fingerprint.R.string.Enrollmentsucceeded)
                    tv_messag.setTextColor(resources.getColor(R.color.green))
                    launchScreen(DashboardActivity().javaClass, true);
                    fingercamera.isregisted = true
                    setallbuttontrue()
                }

                -1 -> {
                    tv_messag.setTextColor(resources.getColor(R.color.red))
                    tv_messag.setText(getString(R.string.registeration_failed))
                    fingercamera.isregisted = false
                    setallbuttontrue()
                }
                2 -> {
                    tv_messag.setText(getString(com.techshino.fingerprint.R.string.Matchingsucceeded) + fingercamera.score)
                    tv_messag.setTextColor(resources.getColor(R.color.green))
                    setallbuttontrue()
                }
                -2 -> {
                    tv_messag.setText(getString(com.techshino.fingerprint.R.string.Matchingfailed)
                            + " socre : " + fingercamera.score)
                    tv_messag.setTextColor(resources.getColor(R.color.red))

                    var bmFinger: Bitmap = BitmapFactory.decodeByteArray(
                            com.techshino.fingerprint.FingerPreview.grey_bmp_buffer, 0, 256 * 360 + 1024 + 54)
                    if (bmFinger != null)
                        iv_finger.setImageBitmap(bmFinger)
                    tv_messag.setText("NFIQ: " + com.techshino.fingerprint.FingerPreview.nfiqValue.toString())
                    tv_messag.setTextColor(resources.getColor(R.color.red))
                    setallbuttontrue()
                }
                3 -> {
                    tv_messag.setText(com.techshino.fingerprint.R.string.founduvcdevice)
                    tv_messag.setTextColor(resources.getColor(R.color.green))

                    setallbuttontrue()
                }
                -3 -> {
                    tv_messag.setText(getString(R.string.no_finger_print))

                    tv_messag.setTextColor(resources.getColor(R.color.red))

                }
                10 -> {
                    tv_messag.setText(com.techshino.fingerprint.R.string.readytoreadagain)
                    tv_messag.setTextColor(resources.getColor(R.color.font_dark))
                }
                -10 -> {
                    tv_messag.setText(com.techshino.fingerprint.R.string.movefingeraway)
                    tv_messag.setTextColor(resources.getColor(R.color.font_dark))
                }
                -13 -> {
                    tv_messag.setText(com.techshino.fingerprint.R.string.outoftime)
                    tv_messag.setTextColor(resources.getColor(R.color.red))
                    setallbuttontrue()
                }
                101 -> {
                    tv_messag.setText(com.techshino.fingerprint.R.string.twolefttoreg)
                    tv_messag.setTextColor(resources.getColor(R.color.font_dark))
                }
                102 -> {
                    tv_messag.setText(com.techshino.fingerprint.R.string.onelefttoreg)
                    tv_messag.setTextColor(resources.getColor(R.color.font_dark))
                }
            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mUsbReceiver)
    }

    private fun setallbuttontrue() {
// useless for now
    }

    private fun setallbuttonfalse() {
        iv_finger.setEnabled(false)
        iv_finger.setEnabled(false)
        iv_finger.setEnabled(false)
    }

    private fun detectUsbDeviceAttach() {
        // listen usb device attach
        val usbDeviceStateFilter = IntentFilter()

        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)
        usbDeviceStateFilter
                .addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED)
        usbDeviceStateFilter
                .addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED)

        registerReceiver(mUsbReceiver, usbDeviceStateFilter)

    }

}

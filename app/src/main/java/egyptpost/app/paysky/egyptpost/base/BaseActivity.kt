package egyptpost.app.paysky.egyptpost.base

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.text.TextUtilsCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.LayoutDirection
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import butterknife.ButterKnife
import com.airbnb.lottie.LottieAnimationView
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.TedPermission
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.extentions.setNavNonBackMode
import egyptpost.app.paysky.egyptpost.base.utils.CSnackbar
import egyptpost.app.paysky.egyptpost.base.utils.Constants
import maes.tech.intentanim.CustomIntent.customType
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.*

abstract class BaseActivity : AppCompatActivity(), PermissionListener {
    var toolbar: Toolbar? = null
    lateinit var loadingView: LottieAnimationView
    open var theme: Int? = null
    var backBtn: ImageView? = null

    public var fragmentCountsChanged: onFragmentCountsChanged? = null

    public fun setFragmentCountListener(fragmentCountsChanged: onFragmentCountsChanged) {
        this.fragmentCountsChanged = fragmentCountsChanged
    }


    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        handleLanguage()
        setContentView(getLayoutId())
        bindViews()
        theme?.let { setTheme(it) } ?: run {
            setTheme(R.style.BaseTheme)
        }
        setupToolbar()


    }

    fun isRtl(): Boolean {
        return TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == LayoutDirection.RTL
    }

    internal fun playLoading() {
        loadingView.setVisibility(View.VISIBLE)
        loadingView.playAnimation()
    }

    internal fun pauseLoading() {
        loadingView.setVisibility(View.GONE)
        loadingView.pauseAnimation()
    }

    internal fun playLoading(lottieFile: String) {
        loadingView.setAnimation(lottieFile, LottieAnimationView.CacheStrategy.Strong)
        loadingView.setVisibility(View.VISIBLE)
        loadingView.playAnimation()
    }


    internal fun checkOtherPermissions() {
        TedPermission.with(this)
                .setPermissionListener(this)
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_CONTACTS,
                        Manifest.permission.SYSTEM_ALERT_WINDOW)
                .check()
    }

    fun setupToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar?.let {
            setSupportActionBar(toolbar)
            supportActionBar?.title = ""
            backBtn = toolbar?.findViewById(R.id.tb_nav)
            backBtn?.setOnClickListener {
                onBackPressed()
            }

        }


    }

    fun setUpLoading() {
        loadingView = findViewById(R.id.loading)
        if (loadingView != null) {
        }
    }


    protected fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    protected fun showToast(msg: Int) {
        Toast.makeText(this, resources.getString(msg), Toast.LENGTH_SHORT).show()
    }


    protected fun showSnackbar(root: View, msg: String, backgroundColor: Int) {
        CSnackbar.SnackBuilder(root, msg)
                .setMessageColor(Color.WHITE)
                .setBackroundColor(backgroundColor)
                .setMessageTextSize(18)
                .setDuration(Snackbar.LENGTH_INDEFINITE)
                .setDismissAction(true)
                .buildSnackBar()
                .show()

    }

    protected fun getSnackbar(root: View, msg: String, backgroundColor: Int): Snackbar {
        return CSnackbar.SnackBuilder(root, msg)
                .setMessageColor(Color.WHITE)
                .setBackroundColor(backgroundColor)
                .setMessageTextSize(18)
                .setDuration(Snackbar.LENGTH_LONG)
                .setDismissAction(true)
                .buildSnackBar()

    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    public fun launchScreen(cls: Class<*>, finishFlag: Boolean = false, exit: Boolean = false) {
        var intent = Intent(this, cls)

        if (finishFlag) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        if (!exit && isRtl() || exit && !isRtl()) {
            startActivity(Intent(this, cls))
            customType(this, Constants.RIGHT_TO_LEFT)
        } else {
            startActivity(Intent(this, cls))
            customType(this, Constants.LEFT_TO_RIGHT)
        }


        if (finishFlag)
            finish()

    }

    public fun launchScreenWithIntent(intent: Intent, finishFlag: Boolean) {

        startActivity(intent)
        if (finishFlag)
            finish()


    }

    public fun launchActivity(intent: Intent?) {
        if (intent != null) {
            startActivity(intent)
        }
    }


    internal fun handleLanguage() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val lang = preferences.getString("lang", Locale.getDefault().language)
        val locale = Locale(lang!!)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        if (lang == "ar")
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_RTL
        else
            window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
        resources.updateConfiguration(config, resources.displayMetrics)

    }

    private fun bindViews() {
        ButterKnife.bind(this)
    }


    /**
     * @return The layout id that's gonna be the activity view.
     */
    protected abstract fun getLayoutId(): Int

    override fun onPermissionGranted() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPermissionDenied(deniedPermissions: ArrayList<String>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    internal fun replaceFragment(fragment: Fragment, addOldToBackStack: Boolean) {
        val transaction = supportFragmentManager.beginTransaction();

        if (isRtl()) {
            transaction.setCustomAnimations(
                    R.anim.enter_from_left, R.anim.exit_to_right,
                    R.anim.enter_from_right, R.anim.exit_to_left
            );
        } else {
            transaction.setCustomAnimations(
                    R.anim.enter_from_right, R.anim.exit_to_left,
                    R.anim.enter_from_left, R.anim.exit_to_right
            );
        }

        transaction.replace(R.id.fragment_frame, fragment)


        if (addOldToBackStack) {
            transaction.addToBackStack(fragment.javaClass.simpleName);
            transaction.commit();
            var fragments = supportFragmentManager.backStackEntryCount

            fragmentCountsChanged?.onCountChanged(fragments)
        } else {
            transaction.commit();
            var fragments = supportFragmentManager.backStackEntryCount
            fragments++
            fragmentCountsChanged?.onCountChanged(fragments)
        }

    }

    fun ActionBack() {
        val fragments = supportFragmentManager.backStackEntryCount
        if (isRtl()) {
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        } else {
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
        if (fragments == 1 || fragments == 0) {
            toolbar?.setNavNonBackMode(true)
        } else {
            if (supportFragmentManager.backStackEntryCount > 1) {
                supportFragmentManager.popBackStack()
            } else {
                super.onBackPressed()
            }
        }

        if (fragments == 2) {

            toolbar?.setNavNonBackMode(false)
        }
        val fragmentsC = supportFragmentManager.backStackEntryCount
        if (fragmentsC == 1) {
            toolbar?.setNavNonBackMode(false)
        }

        fragmentCountsChanged?.onCountChanged(fragments)
    }

    public fun BackBtn(view: View) {
        ActionBack()
    }

    override fun onBackPressed() {
        ActionBack()
        super.onBackPressed()
        val fragments = supportFragmentManager.backStackEntryCount
        if (isRtl()) {
            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        } else {
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        }
        fragmentCountsChanged?.onCountChanged(fragments)


    }

    public interface onFragmentCountsChanged {
        fun onCountChanged(count: Int)
    }
}

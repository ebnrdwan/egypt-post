package egyptpost.app.paysky.egyptpost

import android.app.Application
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

class EgyptPostApp : Application() {


    override fun onCreate() {
        super.onCreate()
    CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/medium.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build());
    }
}
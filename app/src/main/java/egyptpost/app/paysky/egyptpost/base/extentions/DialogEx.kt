package egyptpost.app.paysky.egyptpost.base.extentions

import android.app.Dialog
import android.view.View
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

fun Dialog.animate() {
    YoYo.with(Techniques.Tada)
            .duration(700)
            .repeat(5)
            .playOn(this as View)

}
package egyptpost.app.paysky.egyptpost.view.activity

import android.os.Bundle
import android.os.Handler
import com.airbnb.lottie.LottieAnimationView
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity


class SplashActivity : BaseActivity() {
lateinit var loadinView: LottieAnimationView;
    private val handler: Handler = Handler();
    private val runnable = Runnable {
        launchScreen(LoginActivity().javaClass,true);
    };

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.SplashThem)
        loadinView = findViewById(R.id.loading)

        handler.postDelayed(runnable, 1000);

    }
}

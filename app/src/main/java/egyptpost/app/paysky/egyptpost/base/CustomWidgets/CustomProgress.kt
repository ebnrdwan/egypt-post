package egyptpost.app.paysky.egyptpost.base.CustomWidgets

import android.app.Activity
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.Unbinder
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo
import egyptpost.app.paysky.egyptpost.R

class CustomProgress : ConstraintLayout {

    internal var mainView: View? = null
    @BindView(R.id.ic_add)
    internal var addBtn: ImageView? = null
    @BindView(R.id.ic_order)
    internal var orderBtn: ImageView? = null
    @BindView(R.id.ic_shipping_details)
    internal var shippingDetails: ImageView? = null
    lateinit var unbinder: Unbinder
    internal var context: Context
    internal var activity: Activity


    constructor(context: Context) : super(context) {
        this.context = context
        activity = context as Activity
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
        this.context = context
        activity = context as Activity


    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
        this.context = context
        activity = context as Activity

    }


    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()

    }

    internal fun init() {
        val inflater = LayoutInflater.from(getContext())
        mainView = inflater.inflate(R.layout.linear_progress, this)
        addBtn = mainView?.findViewById(R.id.ic_add)
        orderBtn = mainView?.findViewById(R.id.ic_order)
        shippingDetails = mainView?.findViewById(R.id.ic_shipping_details)

    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width = measuredWidth
        val hight = measuredHeight / 1
        setMeasuredDimension(width, hight)
    }


    internal fun setStep(step: Int) {
        when (step) {
            0, 1 -> {
                animateClick(addBtn)
                addBtn?.setImageResource(R.drawable.ic_orderadeddprogress)
                orderBtn?.setImageResource(R.drawable.ic_deails_add_progress)
                shippingDetails?.setImageResource(R.drawable.ic_shipping_add)
            }

            2 -> {
                animateClick(orderBtn)
                addBtn?.setImageResource(R.drawable.ic_orderadeddprogress)
                orderBtn?.setImageResource(R.drawable.ic_details_added_progress)
                shippingDetails?.setImageResource(R.drawable.ic_shipping_add)
            }

            3 -> {
                animateClick(shippingDetails)
                addBtn?.setImageResource(R.drawable.ic_orderadeddprogress)
                orderBtn?.setImageResource(R.drawable.ic_details_added_progress)
                shippingDetails?.setImageResource(R.drawable.ic_shipping_added_progress)
            }
        }
    }

    internal fun animateClick(view: View?) {
        view?.let {
            YoYo.with(Techniques.Swing)
                    .duration(200)
                    .playOn(findViewById(it.id))
        }


    }


}

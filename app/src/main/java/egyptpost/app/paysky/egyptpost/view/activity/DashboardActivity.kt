package egyptpost.app.paysky.egyptpost.view.activity

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import butterknife.ButterKnife
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity
import egyptpost.app.paysky.egyptpost.base.extentions.setNavNonBackMode
import egyptpost.app.paysky.egyptpost.base.extentions.setTransBg
import egyptpost.app.paysky.egyptpost.model.ServiceModel
import egyptpost.app.paysky.egyptpost.view.activity.listener.OnServiceItemClick
import egyptpost.app.paysky.egyptpost.view.adapter.ServicesAdapter
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : BaseActivity(), OnServiceItemClick {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ButterKnife.bind(this)
        setupToolbar()
        toolbar?.setTransBg(this)
        toolbar?.setNavNonBackMode(true)

        val servicesAdapter = ServicesAdapter(this, this);
        post_rec.adapter = servicesAdapter;
        post_rec.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        servicesAdapter.updateData(setupLists(1))

        val financialAdapter = ServicesAdapter(this, this);
        financial_rec.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        financial_rec.adapter = financialAdapter
        financialAdapter.updateData(setupLists(2))


        val valuesAdapter = ServicesAdapter(this, this);
        value_rec.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        value_rec.adapter = valuesAdapter;
        valuesAdapter.updateData(setupLists(3));

    }

    override fun getLayoutId(): Int {
        return R.layout.activity_dashboard
    }

    fun setupLists(type: Int): ArrayList<ServiceModel> {
        val postalList = arrayListOf<ServiceModel>();
        when (type) {
            1 -> {
                postalList.add(ServiceModel(1, "Pick up", R.drawable.ic_pickup, R.color.orange_label))
                postalList.add(ServiceModel(1, "Deliver", R.drawable.ic_dliver, R.color.orange_label))
                return postalList
            }
            2 -> {
                postalList.add(ServiceModel(2, "Cash Deposit", R.drawable.ic_deposite, R.color.blue_label))
                postalList.add(ServiceModel(2, "Cash Withdraw", R.drawable.ic_withdraw, R.color.blue_label))
                postalList.add(ServiceModel(2, "Tax Payment", R.drawable.ic_tax_payment, R.color.blue_label))
                return postalList
            }
            else -> {
                postalList.add(ServiceModel(3, "Civil Service", R.drawable.ic_civil_service, R.color.green_label))
                postalList.add(ServiceModel(3, "Gov. Transfers", R.drawable.ic_gov_trans, R.color.green_label))
                postalList.add(ServiceModel(3, "Bill Payment", R.drawable.ic_bill_payment, R.color.green_label))
                return postalList
            }
        }

    }

    override fun onServiceItemClick(model: ServiceModel, position: Int) {
        when (model.type) {
            1 -> {
                when (position) {
                    0 -> {
                        launchScreen(PickOrderActivity().javaClass);
                    }
                    1 -> {
                        // Todo add deliver
//                        launchScreen(PickOrderActivity().javaClass);
                    }

                }

            }
            2 -> {

                when (position) {
                    0 -> {
                        launchScreen(DepositeActivity().javaClass)
                    }
                    1 -> {
                        // Todo add deliver
                        launchScreen(WithdrawActivity().javaClass)
                    }

                }


            }
            2 -> {

            }
        }
    }


}

package egyptpost.app.paysky.egyptpost.base.utils

import android.support.design.widget.Snackbar
import android.view.View
import android.widget.TextView

class CSnackbar {

    class SnackBuilder(internal var view: View, internal var message: String) {
        internal var snackbar: Snackbar? = null
        internal var duration = 0
        internal var actionButtonText: String? = null
        internal var messageTextColor = 0
        internal var messageTextSize = 0
        internal var backgroudColor = 0
        private var isAction = false

        fun setMessageColor(messageTextColor: Int): SnackBuilder {
            this.messageTextColor = messageTextColor
            return this
        }

        fun setDuration(duration: Int): SnackBuilder {
            this.duration = duration
            return this

        }

        fun setMessageTextSize(messageTextSize: Int): SnackBuilder {
            this.messageTextSize = messageTextSize
            return this
        }

        fun setBackroundColor(backgroudColor: Int): SnackBuilder {
            this.backgroudColor = backgroudColor
            return this
        }

        fun setDismissAction(isAction: Boolean): SnackBuilder {
            this.isAction = isAction
            return this
        }

        fun buildSnackBar(): Snackbar {
            if (duration != 0) {
                snackbar = Snackbar.make(this.view, message, duration)
            } else {
                snackbar = Snackbar.make(this.view, message, Snackbar.LENGTH_SHORT)
            }

            if (this.messageTextColor != 0) {
                val sbView = snackbar!!.view
                val textView = sbView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
                textView.setTextColor(messageTextColor)
                textView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            }
            if (this.messageTextSize != 0) {
                val sbView = snackbar!!.view
                val textView = sbView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
                textView.textSize = messageTextSize.toFloat()
                textView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            }
            if (this.backgroudColor != 0) {
                val sbView = snackbar!!.view
                sbView.setBackgroundColor(backgroudColor)
            }

            if (this.isAction)
                snackbar!!.setActionTextColor(this.messageTextColor)
            snackbar!!.setAction("DISMISS") { view ->
                if (snackbar != null)
                    snackbar!!.dismiss()
            }

            return snackbar as Snackbar
        }

    }
}

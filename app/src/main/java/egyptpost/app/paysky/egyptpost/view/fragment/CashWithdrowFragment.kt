package egyptpost.app.paysky.egyptpost.view.fragment


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity
import egyptpost.app.paysky.egyptpost.base.utils.GeneralDialogFragment
import egyptpost.app.paysky.egyptpost.view.activity.ScanIdActivity
import egyptpost.app.paysky.egyptpost.view.dialog.CardScanDialog
import egyptpost.app.paysky.egyptpost.view.dialog.listener.OnCardReadListener
import kotlinx.android.synthetic.main.fragment_cash_withdrow.*

class CashWithdrowFragment : Fragment(), View.OnClickListener, OnCardReadListener, GeneralDialogFragment.OnDialogFragmentClickListener {
    override fun onCancelClicked(dialog: GeneralDialogFragment) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onOkClicked(dialog: GeneralDialogFragment) {

    }


    private lateinit var baseActivity: BaseActivity;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseActivity = activity as BaseActivity;
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cash_withdrow, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scan_id_view.setOnClickListener(this);
        collect_money_view.setOnClickListener(this);
    }


    override fun onClick(v: View?) {
        val viewId = v!!.id;
        when (viewId) {
            R.id.scan_id_view -> {

                baseActivity.launchScreen(ScanIdActivity().javaClass, false)
            }
            R.id.collect_money_view -> {
                CardScanDialog(baseActivity, this).show();
            }

        }
    }

    override fun onReadCardSuccess() {
        val dialogFragment = GeneralDialogFragment(context = activity as Context,
                mlayout = R.layout.dialog_complete,
                listener = this, style = R.style.PauseDialogAnimation)
        dialogFragment.setDialogAttr(positiveBtn = "Finish", negativeBtn = "Print")
        dialogFragment.setPrintButtonBackgroundColor(R.color.color_blue_2)
        dialogFragment.setFinishButtonBackgroundColor(R.color.green_label)
        dialogFragment.show()

    }

    override fun onCardReadFailed() {

    }

}

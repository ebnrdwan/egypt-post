package egyptpost.app.paysky.egyptpost.base.utils

sealed class Constants {
    companion object {
        val primary = "primary"
        val primaryDark = "primaryDark"
        val accent = "accent"
        val DATETIME_FORMATE = "dd MMM yyyy  hh:mm a"
        var LEFT_TO_RIGHT = "left-to-right"
        var RIGHT_TO_LEFT = "right-to-left"
        var BOTTOM_TO_UP = "bottom-to-up"
        var UP_TO_BOTTOM = "up-to-bottom"
        var FADIN_TO_FADOUT = "fadein-to-fadeout"
        var ROTATEOUT_TO_ROTATEIN = "rotateout-to-rotatein"
        val OFFLINE_PLAIN = "Offline pin verified"
        val OFFLINE_ENCRYPTED = "Online pin verified"
        val OFFLINE_WITH_SIGN = "Offline pin verified \n Signature"
        val SIGN = "Signature"
        val SALE_T = "SALE"
        val VOID_T = "VOID"
        val REFUN_T = "REFUND"
        val SETTLEE_T = "SETTLEMENT"
        val SALE_V = "000000"
        val VOID_V = "020000"
        val REFUN_V = "200000"

    }
}
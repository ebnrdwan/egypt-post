package egyptpost.app.paysky.egyptpost.view.activity

import android.os.Bundle
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.BaseActivity
import egyptpost.app.paysky.egyptpost.base.extentions.setTransBg
import egyptpost.app.paysky.egyptpost.view.fragment.DepositFragment

class DepositeActivity : BaseActivity() {
    override fun getLayoutId(): Int {
        return R.layout.activity_deposite
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        toolbar?.setTransBg(this)
        val withdrawFragment = DepositFragment()
        replaceFragment(withdrawFragment, false)
    }
}

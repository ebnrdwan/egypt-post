package egyptpost.app.paysky.egyptpost.view.dialog.listener

interface OnCardReadListener {

    fun onReadCardSuccess();
    fun onCardReadFailed();
}
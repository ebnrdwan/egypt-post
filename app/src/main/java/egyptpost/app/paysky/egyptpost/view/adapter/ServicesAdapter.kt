package egyptpost.app.paysky.egyptpost.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.model.ServiceModel
import egyptpost.app.paysky.egyptpost.view.activity.listener.OnServiceItemClick
import kotlinx.android.synthetic.main.item_service.view.*

class ServicesAdapter(var context: Context, private val serviceItemClick: OnServiceItemClick) : RecyclerView.Adapter<GeneralViewHolder>(), View.OnClickListener {
   var mPosition:Int?=null

    override fun onClick(v: View?) {
        val serviceModel: ServiceModel = v?.getTag(R.id.item) as ServiceModel;
        val positionSelected:Int = datalist.indexOf(serviceModel)
        serviceItemClick.onServiceItemClick(serviceModel, position = positionSelected);

    }

    var datalist: ArrayList<ServiceModel> = ArrayList();
    override fun onCreateViewHolder(viwegroup: ViewGroup, poision: Int): GeneralViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_service, viwegroup, false);
        view.setOnClickListener(this);
        return GeneralViewHolder(view)
    }

    fun updateData(data: ArrayList<ServiceModel>) {
        datalist.clear()
        datalist.addAll(data);
        notifyDataSetChanged()
    }

    fun getItemAtPosition(position: Int): ServiceModel? {
        return datalist.get(position)
    }

    fun deleteItemAtPosition(position: Int) {
        datalist.removeAt(position)
        notifyDataSetChanged()
        notifyItemRemoved(position)
        notifyItemRangeChanged(0, datalist.size)
    }

    override fun getItemCount(): Int {
        return datalist.size
    }

    override fun onBindViewHolder(viewholder: GeneralViewHolder, position: Int) {
        mPosition = position
        val model: ServiceModel? = datalist.get(position)
        viewholder.itemView.iv_icon.setImageResource(model!!.icon)
        viewholder.itemView.iv_label.setBackgroundColor(model.color)
        viewholder.itemView.tv_title.setText(model.name)
        viewholder.itemView.setTag(R.id.item, model);
    }
}

class GeneralViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}
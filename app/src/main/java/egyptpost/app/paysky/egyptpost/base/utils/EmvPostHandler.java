package egyptpost.app.paysky.egyptpost.base.utils;

import android.device.SEManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.urovo.i9000s.api.emv.ContantPara;


import java.util.Hashtable;

import egyptpost.app.paysky.egyptpost.view.activity.CardReadingScreen;

import static egyptpost.app.paysky.egyptpost.view.activity.CardReadingScreen.mEmvApi;
import static egyptpost.app.paysky.egyptpost.view.activity.CardReadingScreen.mHandler;
import static egyptpost.app.paysky.egyptpost.view.activity.CardReadingScreen.mContext;



/**
 * Created by lenovo on 2017/12/14.
 */

public class EmvPostHandler extends Handler {

    public final static int iOnReturnCheckCardResultMag = 8;
    public final static int iOnReturnTransactionResult = 7;
    public final static int ionShowMsg = 6;
    public final static int ionProcessICC = 5;
    //public final static int  ionInputPIN = 4;
    public final static int ONLINE_APPROVE = 11;
    public final static int DECLINE = 12;
    public final static int ABORT = 13;
    public final static int OFFLINE_APPROVE = 14;
    public final static int OTHER_INTERFACE = 15;
    public final static int RETRY = 16;
    public final static int FIND_CARD = 17;
    public final static int REQUEST_ONLINE = 18;
    public final static int SHOW_INFO = 19;
    public final static int SHOW_ERR = 110;
    public final static int REQUEST_ONLINEPIN = 111;
    public final static int PRESENT_CARD = 112;
    public final static int START_NFC = 113;
    public final static int CONTACT_PIN = 114;


    SEManager mSEManager;

    public EmvPostHandler(Looper looper) {
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case iOnReturnCheckCardResultMag:
                String strResult = (String) msg.obj;
                strResult = (String) msg.obj;
                Toast.makeText(CardReadingScreen.mContext, strResult,
                        Toast.LENGTH_SHORT).show();
                break;
            case iOnReturnTransactionResult:
                int i = Log.i("maxlog", "iOnReturnTransactionResult");
                strResult = (String) msg.obj;
                Toast.makeText(CardReadingScreen.mContext, strResult,
                        Toast.LENGTH_SHORT).show();
                break;
            case ionShowMsg:
                strResult = (String) msg.obj;
                Toast.makeText(CardReadingScreen.mContext, strResult,
                        Toast.LENGTH_SHORT).show();
                Log.i("maxlog", "ionShowMsg");

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case ionProcessICC: {
                Hashtable<String, Object> data = new Hashtable<String, Object>();
                data = new Hashtable<String, Object>();
                String amount = (String) msg.obj;
                mEmvApi.updateTerminalSetting("9F1A020356");
                data.put("checkCardMode", ContantPara.CheckCardMode.SWIPE_OR_INSERT);
                data.put("currencyCode", "818");
                if (amount != null)
                    data.put("amount", amount);
                data.put("cashbackAmount", "0");
                data.put("checkCardTimeout", "30");
                // TODO:  start emv
                mEmvApi.startEmv(data);
                Log.i("123", "CardReadingScreen  startEmv return ");
            }
            break;

            case START_NFC:
//                start_kernelCL();//
                break;
            case FIND_CARD:

                mHandler.removeMessages(FIND_CARD);
                Toast.makeText(mContext, "card detected", Toast.LENGTH_SHORT).show();
                break;
            case REQUEST_ONLINE:

                mHandler.removeMessages(REQUEST_ONLINE);
                Toast.makeText(mContext, "request online", Toast.LENGTH_SHORT).show();

                break;
            case SHOW_INFO:
                mHandler.removeMessages(SHOW_INFO);
                Bundle bundle = new Bundle();
                bundle = msg.getData();
                String strMsg = bundle.getString("msg"); //msg.getData().toString();
                //LogUtils.d(strMsg);
                Log.d("applog", "handleMessage 1 " + strMsg);

                Toast.makeText(mContext, strMsg, Toast.LENGTH_SHORT).show();
                break;
            case ONLINE_APPROVE:
                Toast.makeText(mContext, "online approve", Toast.LENGTH_SHORT).show();
                break;
            case DECLINE:
                Toast.makeText(mContext, "decline", Toast.LENGTH_SHORT).show();
                break;
            case ABORT:
                Toast.makeText(mContext, "terminate", Toast.LENGTH_SHORT).show();
                break;
            case OFFLINE_APPROVE:
                Toast.makeText(mContext, "offline approve", Toast.LENGTH_SHORT).show();
                break;
            case OTHER_INTERFACE:
                Toast.makeText(mContext, "try other interface", Toast.LENGTH_SHORT).show();
                break;
            case RETRY:
                Toast.makeText(mContext, "retry", Toast.LENGTH_SHORT).show();
                break;
            case PRESENT_CARD:
                Log.d("applog", "Please Present Card");
                Toast.makeText(mContext, "Please Present Card", Toast.LENGTH_SHORT).show();
                break;
            case SHOW_ERR:
                mHandler.removeMessages(SHOW_ERR);
                strMsg = msg.getData().toString();
                Toast.makeText(mContext, strMsg, Toast.LENGTH_SHORT).show();
                break;
            case CONTACT_PIN:
//                mSEManager.getPinBlockEx(mainParam, mPedInputListenerolp);
                break;

        }
    }

}

package egyptpost.app.paysky.egyptpost.base.utils

import android.app.Dialog
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Geocoder
import android.media.AudioManager
import android.media.RingtoneManager
import android.media.ToneGenerator
import android.os.Environment
import android.os.Handler
import android.support.v4.app.NotificationCompat
import android.support.v4.graphics.BitmapCompat
import android.support.v4.text.TextUtilsCompat
import android.util.Base64
import android.util.LayoutDirection
import android.util.Log
import android.view.View
import egyptpost.app.paysky.egyptpost.R
import id.zelory.compressor.Compressor
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.ParseException
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Abdulrahman Rudwan on 16/03/2018.
 */

object GenericUtility {

    val isRtl: Boolean
        get() = TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == LayoutDirection.RTL

    fun imageToString(uri: String): String {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 20, 20)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        var bm = BitmapFactory.decodeFile(uri, options)
        bm = Bitmap.createScaledBitmap(bm, 200, 200, false)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.WEBP, 70, baos) //bm is the bitmap object
        val b = baos.toByteArray()
        val bitmapByteCount = BitmapCompat.getAllocationByteCount(bm)
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    fun imageToString(context: Context, uri: String): String {
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 20, 20)
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        var bm = BitmapFactory.decodeFile(uri, options)
        try {
            bm = Compressor(context)
                    .setMaxWidth(200)
                    .setMaxHeight(200)
                    .setQuality(60)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).absolutePath)
                    .compressToBitmap(File(uri))
        } catch (e: IOException) {
            e.printStackTrace()
        }

        //        bm = Bitmap.createScaledBitmap(bm, 100, 100, false);
        val baos = ByteArrayOutputStream()
        //        bm.compress(Bitmap.CompressFormat.WEBP, 50, baos); //bm is the bitmap object
        val b = baos.toByteArray()
        val bitmapByteCount = BitmapCompat.getAllocationByteCount(bm)
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    fun calculateInSampleSize(
            options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }

    fun bitMapToString(bitmap: Bitmap): String {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.WEBP, 30, stream)
        return Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT)
    }


    fun drawableToBitmap(drawable: Drawable): Bitmap? {
        var bitmap: Bitmap? = null

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        }

        val canvas = Canvas(bitmap!!)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }


    fun drawableToBase64(drawable: Drawable): String? {
        val bitmap = drawableToBitmap(drawable)
        if (bitmap != null) {
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.WEBP, 50, baos) //bm is the bitmap object
            val b = baos.toByteArray()
            return Base64.encodeToString(b, Base64.DEFAULT)
        } else
            return null

    }

    fun stringToDate(aDate: String?, aFormat: String): Date? {

        if (aDate == null) return null
        val pos = ParsePosition(0)
        val simpledateformat = SimpleDateFormat(aFormat)
        return simpledateformat.parse(aDate, pos)

    }

    fun dateAndTimeToDateTimeMilliSeconds(date: String, time: String): Long {
        val myDate = "$date $time"
        val sdf = SimpleDateFormat(Constants.DATETIME_FORMATE)
        var utilDate = java.util.Date()
        try {
            utilDate = sdf.parse(myDate)
        } catch (pe: ParseException) {
            pe.printStackTrace()
        }

        return utilDate.time
    }


    fun showNotificaiton(notificationId: Int, title: String, body: String, context: Context, pendingIntent: PendingIntent) {
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 170)
        toneGen1.startTone(ToneGenerator.TONE_CDMA_HIGH_PBX_SLS, 500)
        notificationBuilder.setDefaults(Notification.DEFAULT_SOUND)
        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build())


    }

    fun getCompleteAddressString(context: Context?, LATITUDE: Double, LONGITUDE: Double): String {
        var strAdd = ""
        if (context != null) {
            val geocoder = Geocoder(context, Locale.getDefault())
            try {
                val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
                if (addresses != null) {
                    val returnedAddress = addresses[0]
                    val strReturnedAddress = StringBuilder("")

                    for (i in 0..returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                    strAdd = strReturnedAddress.toString()
                    Log.w("My Current address", strReturnedAddress.toString())
                } else {
                    Log.w("My Current address", "No Address returned!")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.w("My Current address", "Canont get Address!")
            }

        }

        return strAdd
    }

    fun timerDelayRemoveView(time: Long, view: View) {
        view.visibility = View.VISIBLE
        val handler = Handler()
        handler.postDelayed({ view.visibility = View.GONE }, time)
    }

    fun timerDelayRemoveViewDialog(time: Long, view: View, dialog: Dialog) {
        view.visibility = View.VISIBLE
        val handler = Handler()
        handler.postDelayed({
            view.visibility = View.GONE
            dialog.dismiss()
        }, time)
    }


    @Throws(JSONException::class)
    fun toMap(`object`: JSONObject): Map<String, Any> {
        val map = HashMap<String, Any>()

        val keysItr = `object`.keys()
        while (keysItr.hasNext()) {
            val key = keysItr.next()
            var value = `object`.get(key)

            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            map[key] = value
        }
        return map
    }

    @Throws(JSONException::class)
    fun toList(array: JSONArray): List<Any> {
        val list = ArrayList<Any>()
        for (i in 0 until array.length()) {
            var value = array.get(i)
            if (value is JSONArray) {
                value = toList(value)
            } else if (value is JSONObject) {
                value = toMap(value)
            }
            list.add(value)
        }
        return list
    }

    @JvmStatic
    fun handleCvmResults(code: String): String {
        var code = code
        code = code.substring(0, 2)

        try {
            when (Integer.valueOf(code)) {
                1 -> return Constants.OFFLINE_PLAIN
                2 -> return Constants.OFFLINE_ENCRYPTED
                3, 4 -> return Constants.OFFLINE_WITH_SIGN
                else -> return Constants.SIGN
            }
        } catch (e: NumberFormatException) {
            return Constants.SIGN
        }

    }




}

package egyptpost.app.paysky.egyptpost.view.activity.listener

import egyptpost.app.paysky.egyptpost.model.ServiceModel

interface OnServiceItemClick {

    fun onServiceItemClick(model: ServiceModel, position: Int);
}
package egyptpost.app.paysky.egyptpost.view.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.view.activity.PickOrderActivity
import kotlinx.android.synthetic.main.fragment_add_order.*


class AddOrderFragment : Fragment() {

    private var pick: PickOrderActivity? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pick = activity as PickOrderActivity


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_add_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pick?.onCountChanged(1)
        order_details_view.setOnClickListener {
            val orderDetailsFragment = OrderDetailsFragment()
            pick?.replaceFragment(orderDetailsFragment, true)
        }
    }

}

package egyptpost.app.paysky.egyptpost.base.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.hardware.usb.UsbManager
import android.os.Handler
import android.os.Message
import android.support.annotation.ColorRes
import android.util.Log
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.techshino.fingerprint.FPConfig
import com.techshino.fingerprint.FingerPreview
import com.techshino.fingerprint.Utils
import egyptpost.app.paysky.egyptpost.R
import kotlinx.android.synthetic.main.dialog_complete.*


class GeneralDialogFragment(context: Context, style: Int, listener: OnDialogFragmentClickListener, mlayout: Int) : Dialog(context, R.style.AppDialogTheme) {

    // interface to handle the dialog click back to the Activity
    private var positive: String = "Finish"
    private var negative: String = "Print"
    private var layout: Int = -1
    private var animationRes: Int = R.style.PauseDialogAnimation
    val onlyBtn: Boolean = false
    lateinit var okBtn: TextView
    var cancelBtn: TextView? = null
    internal lateinit var fingercamera: FingerPreview
    var imageViewFingerPrint: ImageView? = null
    var tvMessage: TextView? = null


    open fun setDialogAttr(positiveBtn: String = positive,
                           negativeBtn: String = negative,
                           animation: Int = animationRes, _layout: Int = layout): GeneralDialogFragment {
        positive = positiveBtn
        negative = negativeBtn
        animationRes = animation
        layout = _layout;

        return this
    }

    interface OnDialogFragmentClickListener {
        fun onOkClicked(dialog: GeneralDialogFragment)
        fun onCancelClicked(dialog: GeneralDialogFragment)

    }


    // Create a Dialog using default AlertDialog builder , if not inflate custom view in onCreateView
    init {
        layout = mlayout
        val lp = WindowManager.LayoutParams()
        window!!.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        lp.copyFrom(window!!.attributes)
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.BOTTOM
        getWindow().attributes.windowAnimations = animationRes
        window!!.attributes = lp

        if (layout != -1)
            setContentView(layout)

        okBtn = findViewById(R.id.btn_go)
        okBtn.let {
            okBtn.setText(positive)
            okBtn.setOnClickListener {
                listener.onOkClicked(this);
                dismiss()
            }

        }

        cancelBtn = findViewById(R.id.btn_cancel)
        cancelBtn?.let {
            cancelBtn?.setText(negative)
            cancelBtn?.setOnClickListener {
                listener.onCancelClicked(this);
                dismiss()
            }
        }


        imageViewFingerPrint = findViewById(R.id.iv_finger)
        tvMessage = findViewById(R.id.tv_messag);
        imageViewFingerPrint?.let {
            imageViewFingerPrint?.setOnClickListener {
                fingercamera.StartExtractFeatureAndImageThread()

            }

            //Algorithm Config for STQC
            FPConfig.setFeatureType(FPConfig.FEATURE_INTERNATIONAL_ISO_19794_2_2011)
            FPConfig.setFingerCode(FPConfig.Unknown)
            FPConfig.setQualitythreshold(FPConfig.NFIQ_GOOD)
            FPConfig.setCompareScorethreshold(1200)

            //root for chmod video 0~15
            Thread(Runnable {
                val path = "/dev/video"
                for (i in 0..14) {
                    Utils.upgradeRootPermission(path + i)
                }
            })
        }



        show()
    }

    public fun setPrintButtonBackgroundColor(@ColorRes color: Int) {
        print_layout.setBackgroundColor(context.resources.getColor(color));
        top_label.setBackgroundColor(context.resources.getColor(color));

    }

    public fun setFinishButtonBackgroundColor(@ColorRes color: Int) {
        finish_layout.setBackgroundColor(context.resources.getColor(color));
    }

    companion object {
        // Create an instance of the Dialog with the input
        fun newInstance(context: Context, listener: GeneralDialogFragment.OnDialogFragmentClickListener, layout: Int): GeneralDialogFragment {
            val frag = GeneralDialogFragment(context, R.style.PauseDialogAnimation, listener, layout)
            return frag
        }
    }


    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        imageViewFingerPrint?.let {


            fingercamera = FingerPreview(context, myHanlder)
            Thread(Runnable {
                fingercamera.Init()
                fingercamera.Init()
            }).start()
            detectUsbDeviceAttach()
            Log.e("app", "onResume")
        }
    }


    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        imageViewFingerPrint?.let {

            fingercamera.isworking = false

            Thread(Runnable { fingercamera?.fingerJNI.TCFP_UnInit() }).start()

            context.unregisterReceiver(mUsbReceiver)

        }

    }

    private fun detectUsbDeviceAttach() {
        // listen usb device attach
        val usbDeviceStateFilter = IntentFilter()

        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED)
        usbDeviceStateFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED)
        usbDeviceStateFilter
                .addAction(UsbManager.ACTION_USB_ACCESSORY_ATTACHED)
        usbDeviceStateFilter
                .addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED)

        context.registerReceiver(mUsbReceiver, usbDeviceStateFilter)

    }

    internal var mUsbReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            val curItentActionName = intent.action

            if (UsbManager.ACTION_USB_DEVICE_ATTACHED == curItentActionName) {
                val path = "/dev/video"
                for (i in 0..14) {
                    Utils.upgradeRootPermission(path + i)
                }
                Thread(Runnable { fingercamera.Init() }).start()


                fingercamera.isworking = false

            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED == curItentActionName) {

                fingercamera.fingerJNI.TCFP_UnInit()
                fingercamera.isworking = false


                fingercamera.isworking = false
            }
        }

    }

    var myHanlder: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                0 -> {
                    val bmFinger: Bitmap
                    bmFinger = BitmapFactory.decodeByteArray(
                            com.techshino.fingerprint.FingerPreview.grey_bmp_buffer, 0, 256 * 360 + 1024 + 54)
                    if (bmFinger != null)
                        imageViewFingerPrint?.setImageBitmap(bmFinger)
                    tvMessage?.setText("NFIQ: " + com.techshino.fingerprint.FingerPreview.nfiqValue.toString())
                    tvMessage?.setTextColor(context.resources.getColor(R.color.font_dark))
                }
                1 -> {
                    tvMessage?.setText(com.techshino.fingerprint.R.string.Enrollmentsucceeded)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.green))
                    fingercamera.isregisted = true
                }

                -1 -> {
                    tvMessage?.setTextColor(context.resources.getColor(R.color.red))
                    tvMessage?.setText(context.getString(R.string.registeration_failed))
                    fingercamera.isregisted = false
                }
                2 -> {
                    tvMessage?.setText(context.getString(com.techshino.fingerprint.R.string.Matchingsucceeded) + fingercamera.score)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.green))

                }
                -2 -> {
                    tvMessage?.setText(context.getString(com.techshino.fingerprint.R.string.Matchingfailed)
                            + " socre : " + fingercamera.score)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.red))

                    var bmFinger: Bitmap = BitmapFactory.decodeByteArray(
                            com.techshino.fingerprint.FingerPreview.grey_bmp_buffer, 0, 256 * 360 + 1024 + 54)
                    if (bmFinger != null)
                        imageViewFingerPrint?.setImageBitmap(bmFinger)
                    tvMessage?.setText("NFIQ: " + com.techshino.fingerprint.FingerPreview.nfiqValue.toString())
                    tvMessage?.setTextColor(context.resources.getColor(R.color.red))

                }
                3 -> {
                    tvMessage?.setText(com.techshino.fingerprint.R.string.founduvcdevice)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.green))

                }
                -3 -> {
                    tvMessage?.setText(context.getString(R.string.no_finger_print))

                    tvMessage?.setTextColor(context.resources.getColor(R.color.red))

                }
                10 -> {
                    tvMessage?.setText(com.techshino.fingerprint.R.string.readytoreadagain)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.font_dark))
                }
                -10 -> {
                    tvMessage?.setText(com.techshino.fingerprint.R.string.movefingeraway)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.font_dark))
                }
                -13 -> {
                    tvMessage?.setText(com.techshino.fingerprint.R.string.outoftime)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.red))

                }
                101 -> {
                    tvMessage?.setText(com.techshino.fingerprint.R.string.twolefttoreg)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.font_dark))
                }
                102 -> {
                    tvMessage?.setText(com.techshino.fingerprint.R.string.onelefttoreg)
                    tvMessage?.setTextColor(context.resources.getColor(R.color.font_dark))
                }
            }

        }
    }


}
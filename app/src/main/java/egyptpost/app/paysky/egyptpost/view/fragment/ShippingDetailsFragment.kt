package egyptpost.app.paysky.egyptpost.view.fragment


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import egyptpost.app.paysky.egyptpost.R
import egyptpost.app.paysky.egyptpost.base.utils.GeneralDialogFragment
import egyptpost.app.paysky.egyptpost.view.activity.PickOrderActivity
import kotlinx.android.synthetic.main.fragment_shipping_details.view.*

class ShippingDetailsFragment : Fragment(), GeneralDialogFragment.OnDialogFragmentClickListener {
    private var pick: PickOrderActivity? = null;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        pick = activity as PickOrderActivity
    }

    override fun onOkClicked(dialog: GeneralDialogFragment) {
        val dialogFragment = GeneralDialogFragment(activity!!, R.style.PauseDialogAnimation, this,
                R.layout.dialog_complete)
        dialog.dismiss()


    }

    override fun onCancelClicked(dialog: GeneralDialogFragment) {
        dialog.dismiss()
    }

    lateinit var rootView: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_shipping_details, container, false)
        pick?.onCountChanged(3)
        rootView.btn_complete_payment.setOnClickListener {

            val dialogFragment = GeneralDialogFragment(context = activity as Context,
                    mlayout = R.layout.dialog_rciept,
                    listener = this, style = R.style.PauseDialogAnimation)

            dialogFragment.show()
        }
        return rootView
    }


}

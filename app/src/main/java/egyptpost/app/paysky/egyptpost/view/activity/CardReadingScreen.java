package egyptpost.app.paysky.egyptpost.view.activity;

import android.content.Context;
import android.device.SEManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;

import com.jniexport.UROPElibJni;
import com.urovo.i9000s.api.emv.CAPK;
import com.urovo.i9000s.api.emv.ContantPara;
import com.urovo.i9000s.api.emv.EmvApi;
import com.urovo.i9000s.api.emv.EmvListener;
import com.urovo.paypassApi.CheckCardListener;
import com.urovo.paypassApi.Funs;
import com.urovo.paypassApi.LogUtils;
import com.urovo.paypassApi.PayPassApi;
import com.urovo.paypassApi.PayPassListener;
import com.urovo.paypassApi.PayPassTransData;
import com.urovo.paypassApi.TermParaData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import butterknife.ButterKnife;
import egyptpost.app.paysky.egyptpost.base.utils.EmvPostHandler;
import egyptpost.app.paysky.egyptpost.base.utils.GenericUtility;
import egyptpost.app.paysky.egyptpost.base.utils.TestEmv;
import egyptpost.app.paysky.egyptpost.model.PosEntryModeHelper;
import egyptpost.app.paysky.egyptpost.model.TrackData;

import static egyptpost.app.paysky.egyptpost.base.utils.EmvPostHandler.CONTACT_PIN;
import static egyptpost.app.paysky.egyptpost.base.utils.EmvPostHandler.REQUEST_ONLINE;
import static egyptpost.app.paysky.egyptpost.base.utils.EmvPostHandler.REQUEST_ONLINEPIN;
import static egyptpost.app.paysky.egyptpost.base.utils.EmvPostHandler.SHOW_INFO;
import static egyptpost.app.paysky.egyptpost.base.utils.EmvPostHandler.START_NFC;


public class CardReadingScreen extends AppCompatActivity {
    public static EmvListener mEmvListener = null;
    public final static String LogTag = "emv";
    public static Bundle mainParam;
    public static SEManager mSEManager;//= new SEManager();
    public static EmvApi mEmvApi = null;
    public static Context mContext = null;
    public static EmvPostHandler mHandler;
    int pinType = 0;

    TrackData trackData;

    String mCurrency;

    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy  hh:mm a");
    PosEntryModeHelper posEntryModeHelper = new PosEntryModeHelper();
    private Handler timeHandler;
    private Runnable timeRunnable;
    Date currentDate = new Date();

    //Variables.
    private String transactionType;
    //Variables.
    private long timeInMillis;
    //GUI.

    private PayPassApi payPassApi = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Log.d("flow", "recieved to cardReading void");
        ButterKnife.bind(this);

        mContext = this;
        mHandler = new EmvPostHandler(mContext.getMainLooper());
        mSEManager = new SEManager();
        mEmvListener = new MyEmvListener();
        mainParam = new Bundle();
        String strPath = "/sdcard" + "/PayPassJar/";
        payPassApi = new PayPassApi(1, strPath);
        payPassApi.checkCard(10000, checkCardListener);
        if (getIntent().hasExtra("transactionType"))
            transactionType = getIntent().getExtras().getString("transactionType");


        extractIntentData();
        mEmvApi = new EmvApi(mEmvListener);
        TestEmv.my_testEmv();

    }

    @Override
    protected void onStart() {
        super.onStart();
        TestEmv.my_testEmv();
    }

    public void start_kernelCL() {
        TermParaData termParaData = new TermParaData();
        termParaData.setTermCode_9F1A("0356"); //0356 Indian //add by max 20180719 for visa
        //termParaData.setTermCap_9F33("E0E9C8");
        termParaData.setTermTpye_9F35("22");
        payPassApi.setTermPara(termParaData);

        final PayPassTransData payPassTransData = new PayPassTransData();
        payPassTransData.setiAmount(200);
        payPassTransData.setiAountBack(0);
        payPassTransData.setAccountType(0);
        payPassTransData.setTransCurrCode(0x0356); //0356 Indian
        payPassTransData.setbTranstype(0);

        new Thread(new Runnable() {
            @Override
            public void run() {
                payPassApi.processPayPassKernel(payPassTransData, payPassListener);

            }
        }).start();
    }


    PayPassListener payPassListener = new PayPassListener() {
        @Override
        public void requestTipsConfirm(String msg) {


            Message tempMsg = new Message();
            tempMsg.what = SHOW_INFO;
            Bundle bundle = new Bundle();

            Log.d("applog", "requestTipsConfirm:" + "SHOW_INFO=" + msg);
            bundle.putString("msg", msg);
            tempMsg.setData(bundle);
            Log.d("applog", "requestTipsConfirm TESTTEST:" + "SHOW_INFO22=" + msg);
            mHandler.sendMessage(tempMsg);


        }

        @Override
        public void onRequestOnline() {


            Log.d("applog", "onRequestOnline ");
            Message tempMsg = new Message();
            tempMsg.what = REQUEST_ONLINE;
            mHandler.sendMessage(tempMsg);
            contactLessOnlinePin();

        }

        @Override
        public void requestImportPin(int type, int lasttimeFlag, String amt) {

            Message tempMsg = new Message();
            tempMsg.what = REQUEST_ONLINEPIN;
            tempMsg.obj = amt;
            mHandler.sendMessage(tempMsg);
            Log.d("applog", "requestImportPin");
            contactLessOnlinePin();

        }

        public void contactLessOnlinePin() {
            Log.d("maxlog", "emv_proc_onlinePin");

            mainParam.putInt("KeyUsage", 3);
            mainParam.putInt("PINKeyNo", 1);
            mainParam.putInt("pinAlgMode", 5);// 5 dupkt

            byte[] tags = {(byte) 0x57};
            byte[] Tlv = new byte[128];
            int taglen = 0;
            byte[] Tlv2 = new byte[128];
            int len = payPassApi.readKernelData(tags, Tlv);
            String cardno;
            int cardtype = 0;

            if (len == 0) {
                //0x9F6B //mstrip track 2 //20180515
                cardtype = payPassApi.GetMstripFlag();
                if (cardtype == 1) {
                    byte[] tags2 = {(byte) 0x9F, (byte) 0x6B};
                    len = payPassApi.readKernelData(tags2, Tlv);
                }
            }

            if (len == 0)
                return;
            else {
                if (cardtype == 0) {
                    taglen = Tlv[1];
                    System.arraycopy(Tlv, 2, Tlv2, 0, taglen);
                } else {
                    taglen = Tlv[2];
                    System.arraycopy(Tlv, 3, Tlv2, 0, taglen);
                }
                cardno = com.urovo.paypassApi.Funs.bytes2HexString(Tlv2, taglen);
                Log.d("applog", cardno);
                int lenCardNo = cardno.indexOf('d');
                char cardnumberArray[] = cardno.toCharArray();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < cardnumberArray.length; i++) {
                    if (cardnumberArray[i] == 'd' ||
                            cardnumberArray[i] == 'D' || cardnumberArray[i] == 'f'
                            || cardnumberArray[i] == 'F') {
                        break;
                    }
                    sb.append(cardnumberArray[i]);
                }
                String finalCardNumber = sb.toString();
                Log.d("applog finalCN : ", finalCardNumber);
                cardno = finalCardNumber;
            }

            if (cardno == null) {
                return;
            }

            Log.d("applog", "cardno222:" + cardno);
            mainParam.putString("cardNo", cardno);
            mainParam.putBoolean("sound", true);
            mainParam.putBoolean("onlinePin", true);
            mainParam.putBoolean("FullScreen", true);
            mainParam.putLong("timeOutMS", 60000);
            mainParam.putString("supportPinLen", "0,4,5,6,7,8,9,10,11,12");
            mainParam.putString("title", "Security Keyboard");
            mainParam.putString("message", "Amount = " + 100);

            Log.d("applog", "getPinBlockEx ");

            Message message = mHandler.obtainMessage(CONTACT_PIN);
            message.sendToTarget();

        }


        @Override
        public void onTransResult(byte result) {
            Message tempMsg = new Message();
            tempMsg.what = result;
            mHandler.sendMessage(tempMsg);
        }

        @Override
        public void onErrorInfor(int erroCode, String strErrInfo) {
            Message tempMsg = new Message();
            tempMsg.what = SHOW_INFO;
            //tempMsg.obj = strErrInfo ;
            Bundle bundle = new Bundle();
            Log.d("applog", "onErrorInfor:" + "SHOW_INFO=" + strErrInfo);
            bundle.putString("msg", strErrInfo);
            tempMsg.setData(bundle);

            mHandler.sendMessage(tempMsg);
        }
    };

    public void successSound() {

    }

    private CheckCardListener checkCardListener = new CheckCardListener() {
        @Override
        public void onFindRFCard() {

            Log.d("PayPassSSSS", "MainActivity    onFindRFCard");

            Message tempMsg = new Message();
            tempMsg.what = START_NFC;
            mHandler.sendMessage(tempMsg);
            start_kernelCL(); //add by max 20180515
        }

        @Override
        public void onTimeout() {
            LogUtils.d("paywav time out。。。。。");


        }

        @Override
        public void onCanceled() {

            Log.d("PayPass", "cancel check card");

        }

        @Override
        public void onError(int erroCode, String strErrInfo) {
            Log.d("PayPass", "cancel check card");

        }

        @Override
        public void onReturnNfcCardData(Hashtable<String, String> hashtable) {
            final String ksn = hashtable.get("KSN");
            final String TrackData = hashtable.get("TRACKDATA");
            final String EmvData = hashtable.get("EMVDATA");
            String equivelanttrack2Data = payPassApi.GetEmvCLTagValue((int) 0x57);
            String cmvList = payPassApi.GetEmvCLTagValue((int) 0x8E);
            String cardHolderName = payPassApi.GetEmvCLTagValue((int) 0x5F20);
            String expiration = payPassApi.GetEmvCLTagValue((int) 0x5F24);
            String currency = payPassApi.GetEmvCLTagValue((int) 0x5F2A);
            String cvmResults = payPassApi.GetEmvCLTagValue((int) 0x9F34);
            String tvl = payPassApi.getEMVjarVers();

            cvmResults = GenericUtility.handleCvmResults(cvmResults);

            posEntryModeHelper.cardReadType = PosEntryModeHelper.CardReadType.EMV;
            if (!TextUtils.isEmpty(equivelanttrack2Data)) {
                trackData = new TrackData(equivelanttrack2Data);
                posEntryModeHelper.trackData = trackData;

                if ((equivelanttrack2Data.charAt(equivelanttrack2Data.length() - 1) == 'f') || (equivelanttrack2Data.charAt(equivelanttrack2Data.length() - 1) == 'F'))
                    equivelanttrack2Data = equivelanttrack2Data.substring(0, equivelanttrack2Data.length() - 1);
                if (equivelanttrack2Data.contains("d"))
                    equivelanttrack2Data = equivelanttrack2Data.replace("d", "=");
            }
            successSound();


// TODO: 8/27/2018 AD get all nfc card data as emv
            String tagValue = payPassApi.GetEmvCLTagValue(0x95);
            if (tagValue != null) {
                Log.d("applog", "tagValue 0x95:" + tagValue);
            }

            tagValue = payPassApi.GetEmvCLTagValue(0x9B);
            if (tagValue != null) {
                Log.d("applog", "tagValue 0x9B:" + tagValue);
            }
            payPassApi.GetMstripFlag();

            Log.d("applog", "onReturnNfcCardData");
            Log.d("applog", "KSN:" + ksn);
            Log.d("applog", "TrackData:" + TrackData);
            Log.d("applog", "EmvData:" + EmvData);
        }

    };

    private void extractIntentData() {
        Bundle bundle = getIntent().getExtras();

    }


    public void doOfflinePin() {
        String cardno = mEmvApi.getValByTag((int) 0x5A);
        if ((cardno.charAt(cardno.length() - 1) == 'f') || (cardno.charAt(cardno.length() - 1) == 'F'))
            cardno = cardno.substring(0, cardno.length() - 1);
        this.mSEManager = new SEManager();
        this.mainParam = new Bundle();
        this.mainParam.putInt("KeyUsage", 3);
        this.mainParam.putInt("PINKeyNo", 1);
        this.mainParam.putInt("pinAlgMode", 1);
        this.mainParam.putString("cardNo", cardno);
        this.mainParam.putBoolean("sound", true);
        this.mainParam.putBoolean("onlinePin", false);
        this.mainParam.putBoolean("FullScreen", true);
        this.mainParam.putLong("timeOutMS", 60000L);
        this.mainParam.putString("supportPinLen", "0,4,6,8,10,12");
        this.mainParam.putString("title", "Security Keyboard");
        this.mainParam.putString("message", "please input password ");
        this.pinType = 1;
        this.mSEManager.getPinBlockEx(this.mainParam, mPedInputListenerolp);
    }

    public static SEManager.PedInputListener mPedInputListenerolp = new SEManager.PedInputListener() {
// TODO: get Bin Block creation results

        @Override
        public void onChanged(int i, int i1, byte[] bytes) {
            int result = i;
            int length = i1;
            Log.i("maxlog", "bundle.getByteArray onChanged");
            if (result == 0) {
                // TODO: Bin block created // way to go for online pin
                Log.i("maxlog", "bundle.getByteArray ");

                //emv_proc_onlinePin();

                final byte[] pinBlock = bytes;//bundle.getByteArray("pinBlock");
                //final byte[] ksn = bundle.getByteArray("ksn");

                if (pinBlock.length > 0 && pinBlock.length <= 4)
                    Log.i("maxlog", "bundle.getByteArray pinBlock:" + Funs.bytes2HexString(pinBlock, 4));
                else if (pinBlock.length >= 8)
                    Log.i("maxlog", "bundle.getByteArray pinBlock:" + Funs.bytes2HexString(pinBlock, 8));
                mEmvApi.sendPinEntryResult("");
            } else {
                Log.i("maxlog", "result = " + result + " length = " + length);
            }
            if (result == 1) {
                // TODO: Bin block not created // cancel go to next process in CVMLIST

                mEmvApi.cancelPinEntry();
                mEmvApi.emv_proc_offplain();

            }
        }
    };


    //
    public void doOnlinePin() {
        Log.i("maxlog", "doOnlinePin");

        //mSEManager  = new SEManager();
        mainParam.putInt("KeyUsage", 0x02);
        mainParam.putInt("PINKeyNo", 1);
        mainParam.putInt("pinAlgMode", 4);// 5 dupkt

        String cardno = mEmvApi.getValByTag((int) 0x5A);
        if ((cardno.charAt(cardno.length() - 1) == 'f') || (cardno.charAt(cardno.length() - 1) == 'F'))
            cardno = cardno.substring(0, cardno.length() - 1);

        Log.i("maxlog", "doOnlinePin cardno " + cardno);
        mainParam.putString("cardNo", cardno);
        mainParam.putBoolean("sound", true);
        mainParam.putBoolean("onlinePin", true);
        mainParam.putBoolean("FullScreen", true);
        mainParam.putLong("timeOutMS", 60000);
        mainParam.putString("supportPinLen", "0,4,5,6,7,8,9,10,11,12");
        mainParam.putString("title", "Security Keyboard");
        mainParam.putString("message", "please input password ");
        Log.i("maxlog", "getPinBlockEx ");

        // TODO: create Bin Block
        mSEManager.getPinBlockEx(mainParam, mPedInputListenerolp);
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    class MyEmvListener implements EmvListener {

        @Override
        public void onRequestTerminalTime() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestTerminalTime");
        }

        @Override
        public void onRequestSetAmount() {
            // TODO: show dialog to must set amount
            Log.i(LogTag, "TestEmvActivityMain  onRequestSetAmount");
        }

        @Override
        public void onReturnAmountConfirmResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnAmountConfirmResult");
        }

        @Override
        public void onWaitingForCard(ContantPara.CheckCardMode checkCardMode) {
            Log.i(LogTag, "TestEmvActivityMain  onWaitingForCard checkCardMode=" + checkCardMode);
        }

        @Override
        public void onReturnCancelCheckCardResult(boolean isSuccess) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCancelCheckCardResult isSuccess=" + isSuccess);
            UROPElibJni.CheckKeyStatus();

        }

        @Override
        public void onReturnCheckCardResult(ContantPara.CheckCardResult checkCardResult, Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCheckCardResult checkCardResult =" + checkCardResult);
            Log.d(LogTag, hashtable.toString());
            if (checkCardResult == ContantPara.CheckCardResult.MSR) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.ionShowMsg;
                tempMsg.obj = " MAG CARD SWIPE SUCCESS  !";
                mHandler.sendMessage(tempMsg);
            } else if (checkCardResult == ContantPara.CheckCardResult.INSERTED_CARD) {


                // TODO: 1- detected the card insert
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.ionShowMsg;
                tempMsg.obj = " ICC INSERTED  !";
                mHandler.sendMessage(tempMsg);//ionShowMsg
                Message tempMsg2 = mHandler.obtainMessage();
                tempMsg2.what = EmvPostHandler.ionProcessICC;
                mHandler.sendMessage(tempMsg2);//ionProcessICC

            } else if (checkCardResult == ContantPara.CheckCardResult.NEED_FALLBACK) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.ionShowMsg;
                tempMsg.obj = " NEED_FALLBACK !";
                mHandler.sendMessage(tempMsg);

            } else if (checkCardResult == ContantPara.CheckCardResult.BAD_SWIPE) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.ionShowMsg;
                tempMsg.obj = " BAD_SWIPE !";
                mHandler.sendMessage(tempMsg);
            } else if (checkCardResult == ContantPara.CheckCardResult.NOT_ICC) {
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.iOnReturnCheckCardResultMag;
                tempMsg.obj = " NOT_ICC, A chip card is swiped. !";
                mHandler.sendMessage(tempMsg);

                Log.i("maxlog", "runOnUiThread-----NOT_ICC");
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.i("maxlog", "3000 ms ok-----------");

        }

        @Override
        public void onRequestSelectApplication(ArrayList<String> arrayList) {
            // TODO: 2- got list of applications
            Log.i(LogTag, "TestEmvActivityMain  onRequestSelectApplication");
            int i;
            for (i = 0; i < arrayList.size(); i++) {
                Log.d(LogTag, "app name " + i + " : " + arrayList.get(i));

            }
            //block

            if (i == 1)
                mEmvApi.selectApplication(0);
            else
                mEmvApi.selectApplication(0);//multi app`

            // break;
        }

        @Override
        public void onError(ContantPara.Error error, String s) {
            Log.i(LogTag, "TestEmvActivityMain  onError:" + error);
            Log.i(LogTag, "TestEmvActivityMain  s:" + s);
        }

        @Override
        public void onRequestPinEntry(ContantPara.PinEntrySource pinEntrySource) {

            // TODO: 3-  request pin entry  for online pin
            Log.i(LogTag, "TestEmvActivityMain  onRequestPinEntry");

            if (pinEntrySource == ContantPara.PinEntrySource.KEYPAD) {

//                doOnlinePin();
                doOfflinePin();
                Log.i(LogTag, "TestEmvActivityMain  doOnlinePin over");
            } else {

            }

        }

        public void onRequestConfirmCardno() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestConfirmCardno-----------");
            mEmvApi.sendConfirmCardnoResult(true);

        }

        @Override
        public void onRequestFinalConfirm() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestFinalConfirm-----------");
            mEmvApi.sendFinalConfirmResult(true);

        }

        @Override
        public void onRequestOnlineProcess(String cardTlvData, String dataKsn) {

            String Track2 = mEmvApi.getValByTag((int) 0x57);
            String equivelanttrack2Data = mEmvApi.getValByTag((int) 0x57);
            String cmvList = mEmvApi.getValByTag((int) 0x8E);
            String cardHolderName = mEmvApi.getValByTag((int) 0x5F20);
            String expiration = mEmvApi.getValByTag((int) 0x5F24);
            String currency = mEmvApi.getValByTag((int) 0x5F2A);
            String cvmResults = mEmvApi.getValByTag((int) 0x9F34);
            cvmResults = GenericUtility.handleCvmResults(cvmResults);

            if (!TextUtils.isEmpty(equivelanttrack2Data)) {
                trackData = new TrackData(equivelanttrack2Data);
                posEntryModeHelper.trackData = trackData;

                if ((equivelanttrack2Data.charAt(equivelanttrack2Data.length() - 1) == 'f') || (equivelanttrack2Data.charAt(equivelanttrack2Data.length() - 1) == 'F'))
                    equivelanttrack2Data = equivelanttrack2Data.substring(0, equivelanttrack2Data.length() - 1);
                if (equivelanttrack2Data.contains("d"))
                    equivelanttrack2Data = equivelanttrack2Data.replace("d", "=");
            }
            posEntryModeHelper.cardReadType = PosEntryModeHelper.CardReadType.EMV;


            // TODO: 4- got card data process online
            Log.i(LogTag, "TestEmvActivityMain  onRequestOnlineProcess");
            //send s to sever
            Log.i(LogTag, "TestEmvActivityMain  onRequestOnlineProcess iccdata:" + cardTlvData);
            Log.i(LogTag, "TestEmvActivityMain  onRequestOnlineProcess dataKsn:" + dataKsn);

            /* set icc data on the text*/
//            text1.setText("iccdata:" + cardTlvData + "  KSN:" + dataKsn);

            Log.d("EMVTAG", "TLVDATA: " + cardTlvData);
            Log.d("EMVTAG", "KSN :" + dataKsn);
            Log.d("EMVTAG", "Track2Data : " + equivelanttrack2Data);

            // TODO: wha-1 =  what is the responseData
            mEmvApi.sendOnlineProcessResult(cardTlvData);

        }

        @Override
        public void onReturnBatchData(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnBatchData");
        }

        @Override
        public void onReturnReversalData(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnReversalData");
        }

        @Override
        public void onReturnTransactionResult(ContantPara.TransactionResult transactionResult) {
// TODO: handle online pin
            Log.i(LogTag, "TestEmvActivityMain  onReturnTransactionResult  transactionResult=" + transactionResult);
            //mHandler = new EmvPostHandler(mContext.getMainLooper());
            if (transactionResult == ContantPara.TransactionResult.APPROVED) {
                Log.i(LogTag, "TestEmvActivityMain onReturnTransactionResult  APPROVED");

                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.iOnReturnTransactionResult;
                tempMsg.obj = " TransactionResult = APPROVED !";

                Log.d("EMVTAG", "MAG_TRACK2 : " + tempMsg.obj);

                mHandler.sendMessage(tempMsg);
            } else if (transactionResult == ContantPara.TransactionResult.DECLINED) {
                Log.i(LogTag, "TestEmvActivityMain onReturnTransactionResult  DECLINED");

                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.iOnReturnTransactionResult;
                tempMsg.obj = " TransactionResult = DECLINED !";
//                String str = text1.getText().toString();
//                text1.setText(str + "   " + tempMsg.obj);
                Log.d("EMVTAG", "MAG_RESULTS : " + tempMsg.obj);


                mHandler.sendMessage(tempMsg);
//                mEmvApi.doOfflinePin();
            }

            if (transactionResult == ContantPara.TransactionResult.TERMINATED) {
                Log.i(LogTag, " TestEmvActivityMain onReturnTransactionResult  TERMINATED");
                Message tempMsg = mHandler.obtainMessage();
                tempMsg.what = EmvPostHandler.iOnReturnTransactionResult;
                tempMsg.obj = " TransactionResult = TERMINATED !";
                mHandler.sendMessage(tempMsg);
            }

        }

        @Override
        public void onRequestDisplayText(ContantPara.DisplayText displayText) {
            Log.i(LogTag, "TestEmvActivityMain  onRequestDisplayText");
        }

        @Override
        public void onRequestClearDisplay() {
            Log.i(LogTag, "TestEmvActivityMain  onRequestClearDisplay");
        }

        @Override
        public void onReturnEnableInputAmountResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEnableInputAmountResult");
        }

        @Override
        public void onReturnDisableInputAmountResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnDisableInputAmountResult");
        }

        @Override
        public void onReturnAmount(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnAmount");
        }

        @Override
        public void onReturnEnableAccountSelectionResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEnableAccountSelectionResult");
        }

        @Override
        public void onReturnAccountSelectionResult(ContantPara.AccountSelectionResult accountSelectionResult, int i) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnAccountSelectionResult");
        }

        @Override
        public void onReturnEmvCardDataResult(boolean b, String s) {

            Log.i(LogTag, "TestEmvActivityMain  onReturnPinEntryResult");
        }

        @Override
        public void onReturnEmvCardNumber(boolean b, String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEmvCardDataResult");
        }

        @Override
        public void onReturnPowerOnIccResult(boolean b, String s, String s1, int i) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEmvCardNumber");
        }

        @Override
        public void onReturnPowerOffIccResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEncryptPinResult");
        }

        @Override
        public void onReturnApduResult(boolean b, Hashtable<String, Object> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEncryptDataResult");
        }

        @Override
        public void onReturnReadTerminalSettingResult(ContantPara.TerminalSettingStatus terminalSettingStatus, String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnPowerOnIccResult");
        }

        @Override
        public void onReturnUpdateTerminalSettingResult(ContantPara.TerminalSettingStatus terminalSettingStatus) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnPowerOffIccResult");
        }

        @Override
        public void onReturnCAPKList(List<CAPK> list) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnApduResult");
        }

        @Override
        public void onReturnCAPKDetail(CAPK capk) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnInjectSessionKeyResult");
        }

        @Override
        public void onReturnCAPKLocation(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnReadTerminalSettingResult");
        }

        @Override
        public void onReturnUpdateCAPKResult(boolean b) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnUpdateTerminalSettingResult");
        }

        @Override
        public void onReturnEmvReportList(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCAPKList");
        }

        @Override
        public void onReturnEmvReport(String s) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCAPKDetail");
        }

        @Override
        public void onReturnReadAIDResult(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnCAPKLocation");
        }

        @Override
        public void onReturnUpdateAIDResult(Hashtable<String, String> hashtable) {
            Log.i(LogTag, "TestEmvActivityMain  onReturnEmvReportList");
        }

    }

}

package egyptpost.app.paysky.egyptpost.base.utils

import android.app.Activity
import android.support.v4.app.DialogFragment


abstract class BaseDialogFragment<T> : DialogFragment() {
    var activityInstance: T? = null
        private set

    override fun onAttach(activity: Activity) {
        activityInstance = activity as T
        super.onAttach(activity)
    }

    override fun onDetach() {
        super.onDetach()
        activityInstance = null
    }
}
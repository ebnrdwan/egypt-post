package egyptpost.app.paysky.egyptpost.model;

/**
 * Created by Ahmed AL Agamy on 8/28/2017.
 */

public class PosEntryModeHelper {
    public TrackData getTrackData() {
        return trackData;
    }

    public void setTrackData(TrackData trackData) {
        this.trackData = trackData;
    }

    public CardReadType getCardReadType() {
        return cardReadType;
    }

    public void setCardReadType(CardReadType cardReadType) {
        this.cardReadType = cardReadType;
    }

    public int getEmvReadFailAttempts() {
        return emvReadFailAttempts;
    }

    public void setEmvReadFailAttempts(int emvReadFailAttempts) {
        this.emvReadFailAttempts = emvReadFailAttempts;
    }

    public boolean isManualOperation() {
        return isManualOperation;
    }

    public void setManualOperation(boolean manualOperation) {
        isManualOperation = manualOperation;
    }

    //Objects.
    public TrackData trackData;
    public CardReadType cardReadType;
    //Variables.
    private int emvReadFailAttempts;
    private boolean isManualOperation;

    public PosEntryModeHelper() {

    }

    public PosEntryModeHelper(TrackData trackData, CardReadType cardReadType) {
        this.trackData = trackData;
        this.cardReadType = cardReadType;
    }


    public boolean isCardSupportEmv() {
        // after = in track2 data expiration date and it is 4 characters after that 3 characters is service code
        // first digit in characters is card support emv or not.
        char c;
        if (trackData.track2 == null) {
            // EMV Card data.
            String track = trackData.tracks[1];
            c = track.charAt(4);
        } else {
            // magnetic card data.
            String[] cardParts = trackData.track2.split("=");
            c = cardParts[1].charAt(4);
        }

        if (c == '2' || c == '6') {
            // card support EMV
            return true;
        }
        return false;
    }


    public boolean acceptMagneticOperation() {
        return cardReadType == CardReadType.MAGNETIC && isCardSupportEmv() || emvReadFailAttempts == 3;
    }

    public String getPosEntryMode() {
        // TODO: 7/10/2018 AD handle get operation Type 
        // card not support emv return 200.
        if (!isCardSupportEmv()) {
            return "022";
        } else if (isManualOperation) return "011";
        else if (emvReadFailAttempts < 3) {
            return "051";
        } else return "108"; // emv read fail 3 times and we accept magnetic operation.

    }

    public void incrementReadEmvFailAttempts() {
        emvReadFailAttempts++;
    }


    public enum CardReadType {
        MAGNETIC, EMV
    }


}
